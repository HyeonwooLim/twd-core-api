package com.skt.twd.cache.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <br> 업무명 : API 메타 정보
 * <br> 설  명 : API 메타 정보 조회
 * <br> 작성일 : 2018. 09. 05
 * @author P127596
 *
 */
@RedisHash("ApiMeta")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ApiMeta {
	/**
	 * API ID
	 */
	@Id
	private String apiId;
	/**
	 * API 명
	 */
	private String apiName;
	/**
	 * API URL
	 */
	private String apiUrl;
	/**
	 * 사용 여부
	 */
	private String useYn;
	/**
	 * API설명
	 */
	private String apiDesc;
	/**
	 * 차단 여부
	 */
	//private String blYn;
	private String isolYn;
	/**
	 * API 차단시작시간
	 */
	//private String apiBlStDtm;
	private String apiIsolStaDtm;
	/**
	 * API 차단종료시간
	 */
	//private String apiBlEdDtm;
	private String apiIsolEndDtm;
	/**
	 * API GROUP 차단시작시간
	 */
	//private String groupBlStDtm;
	private List<String> grpIsolStaDtm;
	/**
	 * API GROUP 차단종료시간
	 */
	//private String groupBlEdDtm;
	private List<String> grpIsolEndDtm;
	/**
	 * 레거시 차단시작시간
	 */
	//private List<String> legacyBlStDtm;
	private List<String> legIsolStaDtm;
	/**
	 * 레거시 차단종료시간
	 */
	//private List<String> legacyBlEdDtm;
	private List<String> legIsolEndDtm;
	/**
	 * HTTP 메소드
	 */
	private String httpMethod;

	@JsonIgnore
	public String getKey() {
		return this.getClass().getSimpleName().toLowerCase();
	}
}
