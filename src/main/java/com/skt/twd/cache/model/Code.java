package com.skt.twd.cache.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <br>
 * 업무명 : 코드 정보 <br>
 * 설 명 : 코드 정보 조회 <br>
 * 작성일 : 2018. 08. 21
 */
@RedisHash("Code")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Code {
	/**
	 * ID
	 */
	@Id
	private String id;
	/**
	 * 코드_대분류
	 */
	private String lCd;
	/**
	 * 코드_중분류
	 */
	private String mCd;
	/**
	 * 유효종료일시
	 */
	private String effStaDtm;
	/**
	 * 유효시작일시
	 */
	private String effEndDtm;
	/**
	 * 코드명
	 */
	private String codeNm;
	/**
	 * 정렬_순번
	 */
	private String orderSeq;
	/**
	 * ALPHA참고필드1
	 */
	private String ref1;
	/**
	 * ALPHA참고필드2
	 */
	private String ref2;
	/**
	 * ALPHA참고필드3
	 */
	private String ref3;
	/**
	 * 비고
	 */
	private String etc;
	/**
	 * 아이콘
	 */
	private String icon;
	/**
	 * 언어코드
	 */
	private String langCd;
	/**
	 * 11번가 인터페이스 값 전송여부
	 */
	private String if11stYn;

}
