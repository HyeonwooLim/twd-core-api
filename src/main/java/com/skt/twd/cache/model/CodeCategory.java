package com.skt.twd.cache.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <br>
 * 업무명 : 코드 분류 정보 <br>
 * 설 명 : 코드 분류 정보 조회 <br>
 * 작성일 : 2018. 08. 21
 */
@RedisHash("CodeCategory")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class CodeCategory {

	/**
	 * 코드_대분류
	 */
	@Id
	private String lCd;

	private List<Code> codes;
}
