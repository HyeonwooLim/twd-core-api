package com.skt.twd.cache.model;

import org.springframework.data.redis.core.RedisHash;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RedisHash("TssMeta")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class TssMeta {
	/**
	 * 로그 생성 여부
	 */
	private String createLogYn;
	
	@JsonIgnore
	public String getKey() {
		return this.getClass().getSimpleName().toLowerCase();
	}
}
