package com.skt.twd.core;

public class ConfigProfiles {
	public static final String LOCAL        = "local";
	public static final String DEVELOPMEMT  = "dev";
	public static final String STAGING  	= "stg";
	public static final String PRODUCTION   = "prd";
}