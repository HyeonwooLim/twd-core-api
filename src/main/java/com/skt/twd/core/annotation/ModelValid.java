package com.skt.twd.core.annotation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.skt.twd.core.mvc.model.ModelValidator;

@Retention(RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = { ModelValidator.class })
public @interface ModelValid {
	String message() default "";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
