package com.skt.twd.core.aop;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.skt.twd.core.mvc.BlockApiComponent;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class BlockApiAspect {
	/**
	 * blockApi
	 */
	@Autowired
	private BlockApiComponent blockApiComponent;

	/**
	 * declaire validation pointcut
	 */
	@Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)"
			+ " || @annotation(org.springframework.web.bind.annotation.PostMapping)"
			+ " || @annotation(org.springframework.web.bind.annotation.PutMapping)"
			+ " || @annotation(org.springframework.web.bind.annotation.DeleteMapping)")
	public void blockApiPoint() {
	}

	@Before("blockApiPoint()")
	public void blockApiBefore(final JoinPoint joinPoint) {
		final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		log.debug("signature.getClass().getSimpleName() : {}", signature.getClass().getSimpleName());

		final Method method = signature.getMethod();
		log.debug("method.getName() : {}, method.getDeclaringClass().getName() : {}", method.getName(), method.getDeclaringClass().getName());

		if(!method.getDeclaringClass().isAnnotationPresent(FeignClient.class)) {

			HttpMethod httpMethod  = null;
			String fullUrl = "";
			String url = "";

			if(method.getDeclaringClass().isAnnotationPresent(RequestMapping.class)) {
				url = String.join("", method.getDeclaringClass().getAnnotation(RequestMapping.class).value());
			}

			if (method.isAnnotationPresent(GetMapping.class)) {
				httpMethod = HttpMethod.GET;
				fullUrl = url + String.join("", method.getAnnotation(GetMapping.class).value());
			} else if (method.isAnnotationPresent(PostMapping.class)) {
				httpMethod = HttpMethod.POST;
				fullUrl = url + String.join("", method.getAnnotation(PostMapping.class).value());
			} else if (method.isAnnotationPresent(PutMapping.class)) {
				httpMethod = HttpMethod.PUT;
				fullUrl = url + String.join("", method.getAnnotation(PutMapping.class).value());
			} else if (method.isAnnotationPresent(DeleteMapping.class)) {
				httpMethod = HttpMethod.DELETE;
				fullUrl = url + String.join("", method.getAnnotation(DeleteMapping.class).value());
			}

			blockApiComponent.check(httpMethod, fullUrl);
		}
	}

	@After("blockApiPoint()")
	public void blockApiAfter(final JoinPoint joinPoint) {
	}
}
