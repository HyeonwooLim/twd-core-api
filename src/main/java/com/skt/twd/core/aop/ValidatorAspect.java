package com.skt.twd.core.aop;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;

import com.skt.twd.core.annotation.ComponentValid;
import com.skt.twd.core.annotation.RequestValid;
import com.skt.twd.core.annotation.ServiceValid;
import com.skt.twd.core.utils.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class ValidatorAspect {
	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * declaire validation pointcut
	 */
	@Pointcut("@annotation(com.skt.twd.core.annotation.RequestValid)"
			+ " || @annotation(com.skt.twd.core.annotation.ServiceValid)"
			+ " || @annotation(com.skt.twd.core.annotation.ComponentValid)")
	public void validPoint() {
	}

	@Before("validPoint()")
	public void validateParameter(final JoinPoint joinPoint) {

		final MethodSignature signature = (MethodSignature) joinPoint.getSignature();

		Arrays.stream(joinPoint.getArgs()).findFirst().ifPresent(object -> {
			final Method method = signature.getMethod();
			if (method.isAnnotationPresent(RequestValid.class)) {
				for (Class<?> clazz : method.getAnnotation(RequestValid.class).value()) {
					valid(object, clazz);
				}
			} else if (method.isAnnotationPresent(ServiceValid.class)) {
				for (Class<?> clazz : method.getAnnotation(ServiceValid.class).value()) {
					valid(object, clazz);
				}
			} else if (method.isAnnotationPresent(ComponentValid.class)) {
				for (Class<?> clazz : method.getAnnotation(ComponentValid.class).value()) {
					valid(object, clazz);
				}
			}
		});
	}

	private void valid(Object object, Class<?> clazz) {
		Validator validator = (Validator) applicationContext.getBean(clazz);
		ValidatorUtil.valid(object, validator);
	}
}
