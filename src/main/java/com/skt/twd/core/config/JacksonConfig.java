package com.skt.twd.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.skt.twd.cache.common.utils.RedisLoaderUtils;
import com.skt.twd.core.mvc.convert.DateTimeModule;
import com.skt.twd.core.utils.JsonUtil;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class JacksonConfig {

	@Bean
	public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
		ObjectMapper objectMapper = builder.createXmlMapper(false).build();

		// Some other custom configuration for supporting Java 8 features
//		objectMapper.registerModule(new Jdk8Module());
//		objectMapper.registerModule(new JavaTimeModule());

		// Use property
//		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

		JsonUtil.setObjectMapper(objectMapper);
		log.debug("JsonUtil set ObejctMapper : {}", objectMapper);

		RedisLoaderUtils.setObjectMapper(objectMapper);
		log.debug("RedisLoaderUtils set ObejctMapper : {}", objectMapper);

		return objectMapper;
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(ObjectMapper objectMapper) {
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();

        // 생성한 모듈을 등록해 준다.
        objectMapper.registerModule(new DateTimeModule());
        jsonConverter.setObjectMapper(objectMapper);

        return jsonConverter;
    }
}
