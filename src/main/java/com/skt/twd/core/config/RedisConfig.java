package com.skt.twd.core.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.data.redis.LettuceClientConfigurationBuilderCustomizer;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.core.RedisTemplate;

import com.skt.twd.cache.CacheConstants.RedisSerializerTypes;
import com.skt.twd.cache.common.utils.RedisLoaderUtils;

import io.lettuce.core.ReadFrom;

/**
 * Redis 자동 설정
 */

@Configuration
@EnableConfigurationProperties(RedisProperties.class)
@ConditionalOnClass({ RedisSentinelConfiguration.class, io.lettuce.core.ClientOptions.class })
@AutoConfigureBefore(RedisAutoConfiguration.class)
public class RedisConfig {

	/**
	 * Redis Client Configuration Builder 사용자 옵션 추가 설정
	 *
	 * @param properties
	 * @return 옵션을 추가한 LettuceClientConfigurationBuilder 객체
	 */
	@Bean
	public LettuceClientConfigurationBuilderCustomizer lettuceClientConfigurationBuilderCustomizerForReadFromSlave(
			final RedisProperties properties) {
		return builder -> {
			if ((properties.getSentinel() != null && !properties.getSentinel().getNodes().isEmpty())
					|| (properties.getCluster() != null && !properties.getCluster().getNodes().isEmpty())) {
				// builder.readFrom(ReadFrom.SLAVE_PREFERRED);
				builder.readFrom(ReadFrom.NEAREST); // core 영역은 쓰기가 거의 일어나지 않기 때문에 master도 read하도록 수정
			}
		};
	}

	/**
	 * Redis 템플릿 RedisSerializer 설정
	 *
	 * @param connectionFactory
	 * @return Redis Template
	 */
	@Bean(name = "redisTemplate")
	@Primary
	public RedisTemplate<String, Object> redisTemplate(@Qualifier("redisConnectionFactory") final RedisConnectionFactory connectionFactory) {
		RedisLoaderUtils.setObjectMapper();
		RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		RedisLoaderUtils.redisTemplateKeySerializer(RedisSerializerTypes.String, template);
		RedisLoaderUtils.redisTemplateValueSerializer(RedisSerializerTypes.GenericJackson2Json, template);
		template.setConnectionFactory(connectionFactory);
		return template;
	}
}
