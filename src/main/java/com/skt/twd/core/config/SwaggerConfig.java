package com.skt.twd.core.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import com.skt.twd.core.AppConfig;
import com.skt.twd.core.ConfigProfiles;

import lombok.extern.slf4j.Slf4j;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
@Profile({ConfigProfiles.LOCAL, ConfigProfiles.DEVELOPMEMT, ConfigProfiles.STAGING, ConfigProfiles.PRODUCTION})
@Slf4j
public class SwaggerConfig {
	@Autowired
	private AppConfig appConfig;

	@Bean
	public Docket apiDocket() {
		log.debug("SwaggerConfig apiDocket start ... {}", appConfig.getActive());
	    return new Docket(DocumentationType.SWAGGER_2)
	            .select()
//	            .apis(RequestHandlerSelectors.any())
	            .apis(RequestHandlerSelectors.basePackage(appConfig.getBasePackage()))
	            .paths(PathSelectors.any())
//	            .paths(PathSelectors.ant("/v2/**"))
	            .build()
	            .apiInfo(getApiInfo());
	}

	private ApiInfo getApiInfo() {
		log.debug("SwaggerConfig getApiInfo start ... {}", appConfig.getActive());
	    return new ApiInfo(
	    		appConfig.getTitle() + " / " + appConfig.getActive(), 			// TITLE
	    		appConfig.getDescription(), 	// DESCRIPTION
	    		appConfig.getApiVersion(), 		// VERSION
	    		appConfig.getTermsServiceUrl(), // TERMS OF SERVICE URL
	            new Contact(appConfig.getContactName(),appConfig.getContactUrl(),appConfig.getContactEmail()), // "NAME","URL","EMAIL"
	            appConfig.getLicense(), 		// LICENSE
	            appConfig.getLicenseUrl(), 		// LICENSE URL
	            Collections.emptyList()
	    );
	}
}
