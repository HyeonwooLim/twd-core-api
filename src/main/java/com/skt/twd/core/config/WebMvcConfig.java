package com.skt.twd.core.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletRegistration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletPathProvider;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.skt.twd.core.AppConfig;
import com.skt.twd.core.filter.HttpRequestWrapperFilter;
import com.skt.twd.core.mvc.interceptor.BlockApiInterceptor;
import com.skt.twd.core.mvc.restclient.FeignCustomRequestInterceptor;
import com.skt.twd.core.mvc.restclient.IRestClientHeaderBinder;
import com.skt.twd.core.utils.CipherManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private BlockApiInterceptor blockApiInterceptor;

	@Autowired
	private AppConfig appConfig;

	@PostConstruct
	public void init() {
		CipherManager.setAppConfig(appConfig);
	}

	@Bean
	public FilterRegistrationBean<HttpRequestWrapperFilter> httpRequestWrapperFilter() {
		FilterRegistrationBean<HttpRequestWrapperFilter> registration = new FilterRegistrationBean<HttpRequestWrapperFilter>();
		registration.setFilter(new HttpRequestWrapperFilter());
		registration.addUrlPatterns("/*");
		registration.setName("httpRequestWrapperFilter");
		registration.setOrder(1);

		return registration;
	}

	@Override
    public void addInterceptors(InterceptorRegistry registry) {
		List<String> pathPatterns = new ArrayList<>();
		pathPatterns.add("/promotion/**");
		pathPatterns.add("/notice/**");
		pathPatterns.add("/wireless/**");
		pathPatterns.add("/nonwireless/**");
		pathPatterns.add("/order/**");
		pathPatterns.add("/customer/**");

        registry.addInterceptor(blockApiInterceptor)
        		.addPathPatterns(pathPatterns);
    }

	@Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource);
        return bean;
    }

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//		ObjectMapper webObjectMapper = objectMapper.copy();
//		webObjectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
//		converters.add(new MappingJackson2HttpMessageConverter(webObjectMapper));
	}

    @Override
    public Validator getValidator() {
        return validator();
    }

    @Bean
    public IRestClientHeaderBinder.DefaultRestClientHeaderBinder DefaultRestTemplateHeaderBinder() {
        return new IRestClientHeaderBinder.DefaultRestClientHeaderBinder();
    }

    @Bean
    @ConditionalOnMissingBean({FeignCustomRequestInterceptor.class})
    public FeignCustomRequestInterceptor CustomFeignRequestInterceptor(){
        return new FeignCustomRequestInterceptor();
    }

	/**
	 * Async 처리시 RequestContextHolder.getRequestAttributes() 에서 HttpRequest 를 얻기 위해
	 * dispatcherServlet.setThreadContextInheritable(true);  를 추가함
	 * RequestContextHolder.getRequestAttributes()
	 * @see DispatcherServletAutoConfiguration
	 * */
	@Configuration
	@ConditionalOnClass(ServletRegistration.class)
	@EnableConfigurationProperties(WebMvcProperties.class)
	@AutoConfigureBefore(DispatcherServletAutoConfiguration.class)
	protected static class CustomDispatcherServletConfiguration {
		public static final String DEFAULT_DISPATCHER_SERVLET_BEAN_NAME = "dispatcherServlet";
		private final WebMvcProperties webMvcProperties;
		private final ServerProperties serverProperties;

		public CustomDispatcherServletConfiguration(final WebMvcProperties webMvcProperties,
				final ServerProperties serverProperties) {
			this.webMvcProperties = webMvcProperties;
			this.serverProperties = serverProperties;
		}

		@Bean(name = DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
		public DispatcherServlet dispatcherServlet() {
			DispatcherServlet dispatcherServlet = new DispatcherServlet();
			dispatcherServlet.setDispatchOptionsRequest(
					this.webMvcProperties.isDispatchOptionsRequest());
			dispatcherServlet.setDispatchTraceRequest(
					this.webMvcProperties.isDispatchTraceRequest());
			dispatcherServlet.setThrowExceptionIfNoHandlerFound(
					this.webMvcProperties.isThrowExceptionIfNoHandlerFound());
			dispatcherServlet.setThreadContextInheritable(true);
			return dispatcherServlet;
		}

		@Bean
		@ConditionalOnBean(MultipartResolver.class)
		@ConditionalOnMissingBean(name = DispatcherServlet.MULTIPART_RESOLVER_BEAN_NAME)
		public MultipartResolver multipartResolver(final MultipartResolver resolver) {
			// Detect if the user has created a MultipartResolver but named it incorrectly
			return resolver;
		}

		@Bean
		public DispatcherServletPathProvider mainDispatcherServletPathProvider() {
			return () -> CustomDispatcherServletConfiguration.this.serverProperties.getServlet()
					.getPath();
		}

	}
}
