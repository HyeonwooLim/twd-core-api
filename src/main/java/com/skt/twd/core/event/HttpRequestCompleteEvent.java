package com.skt.twd.core.event;

/**
 * Created by alcava00 on 2018. 3. 27..
 */
public class HttpRequestCompleteEvent extends HttpRequestCreateEvent {

	private static final long serialVersionUID = 5236926288538887656L;

	/**
	 * ;
	 * Constructs a prototypical Event.
	 *
	 * @param source The object on which the Event initially occurred.
	 * @throws IllegalArgumentException if source is null.
	 */
	public HttpRequestCompleteEvent(final Object source) {
		super(source);
	}

	private Throwable throwable;

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(final Throwable throwable) {
		this.throwable = throwable;
	}
}
