package com.skt.twd.core.event;

/**
 * Created by alcava00 on 2018. 3. 27..
 */
public class HttpRequestCreateEvent extends HttpRequestEvent{

	private static final long serialVersionUID = 4710810209861229314L;

	/**
	 * Constructs a prototypical Event.
	 *
	 * @param source The object on which the Event initially occurred.
	 * @throws IllegalArgumentException if source is null.
	 */
	public HttpRequestCreateEvent(final Object source) {
		super(source);
	}

}
