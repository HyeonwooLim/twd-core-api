package com.skt.twd.core.event;

import org.aspectj.lang.ProceedingJoinPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.EventObject;

/**
 * Created by alcava00 on 2018. 3. 27..
 */
public class HttpRequestEvent extends EventObject {

	private static final long serialVersionUID = -4679172471556268079L;

	private String targetName;
	private String methodName;
	private long startTime;
	private String requestUri;
	private String httpMethod;
	private HttpServletRequest httpServletRequest;
	private HttpServletResponse httpServletResponse;
	private Object[] args;
	private long elapseTime;
	private String userAgent;
	private String traceID;
	private String spanId;

	/**
	 * Constructs a prototypical Event.
	 *
	 * @param source The object on which the Event initially occurred.
	 * @throws IllegalArgumentException if source is null.
	 */
	public HttpRequestEvent(final Object source) {
		super(source);
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(final String targetName) {
		this.targetName = targetName;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(final String methodName) {
		this.methodName = methodName;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(final long startTime) {
		this.startTime = startTime;
	}

	public HttpServletRequest getHttpServletRequest() {
		return httpServletRequest;
	}

	public void setHttpServletRequest(final HttpServletRequest httpServletRequest) {
		this.httpServletRequest = httpServletRequest;
	}

	public String getRequestUri() {
		return requestUri;
	}

	public void setRequestUri(final String requestUri) {
		this.requestUri = requestUri;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(final String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(final Object[] args) {
		this.args = args;
	}

	public HttpServletResponse getHttpServletResponse() {
		return httpServletResponse;
	}

	public void setHttpServletResponse(final HttpServletResponse httpServletResponse) {
		this.httpServletResponse = httpServletResponse;
	}

	public long getElapseTime() {
		return elapseTime;
	}

	public void setElapseTime(final long elapseTime) {
		this.elapseTime = elapseTime;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(final String userAgent) {
		this.userAgent = userAgent;
	}

	public String getTraceID() {
		return traceID;
	}

	public void setTraceID(final String traceID) {
		this.traceID = traceID;
	}

	public String getSpanId() {
		return spanId;
	}

	public void setSpanId(final String spanId) {
		this.spanId = spanId;
	}

	public HttpRequestEvent setProceedingJoinPoint(final ProceedingJoinPoint pjp){
		this.setArgs(pjp.getArgs());
		this.setMethodName(pjp.getSignature().getName());
		this.setTargetName(pjp.getSignature().getName());
		return this;
	}

}
