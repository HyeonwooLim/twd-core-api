package com.skt.twd.core.exceptions;

import org.springframework.context.NoSuchMessageException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skt.twd.core.mvc.ResponseBase;
import com.skt.twd.core.utils.MessageUtil;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice(value="${spring.application.base-package}", annotations=Controller.class)
@Slf4j
public class GlobalExceptionHandler {

	/**
	 * - VO Valid
	 * - Object Valid
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	public @ResponseBody ResponseEntity<?> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) throws Exception {
		log.error(e.getMessage(), e);

		try {
			String code       = e.getBindingResult().getFieldErrors().stream().findFirst().get().getDefaultMessage();
			String message    = MessageUtil.get(code);
			ErrorVo errorVo = new ErrorVo(code, message);
	
		    return ResponseBase.error(errorVo);
		} catch (NoSuchMessageException ex) {
			return ResponseBase.error(getDefaultErrorVo(e));
		}
	}

	/**
	 * BizException
	 *
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(value = BizException.class)
	public @ResponseBody ResponseEntity<?> bizExceptionErrorHandler(BizException e) throws Exception {
		log.error(e.getMessage(), e);

		try {
			String code = e.getMessage();
			String message = MessageUtil.get(code, e.getMessageArgs());
			ErrorVo errorVo = new ErrorVo(code, message);
			return ResponseBase.error(errorVo);
		} catch (NoSuchMessageException ex) {
			return ResponseBase.error(getDefaultErrorVo(e));
		}
	}

	/**
	 * InterfaceClientException
	 *
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(value = InterfaceClientException.class)
	public @ResponseBody ResponseEntity<?> interfaceClientException(InterfaceClientException e) throws Exception {

		String code = e.getExceptionCode();
		String message = null;

		try {
			message = MessageUtil.get(code, e.getMessageArgs());
		} catch (NoSuchMessageException ex) {
			
			// interface 시스템의 메시지 확인
			message = (e.getOrgMessage() != null) ? e.getOrgMessage() : e.getMessage();
			
			// interface 시스템의 오류코드 표시
			if (e.getOrgExceptionCode() != null) {
				message += " [" + e.getOrgExceptionCode() + "]";
			}

			log.error("{} : code[{}], message[{}], orgCode[{}], orgMessage[{}], messageArgs[{}], interfaceType[{}]",
					e.getClass().getSimpleName(), e.getExceptionCode(), e.getMessage(),
					e.getOrgExceptionCode(), e.getOrgMessage(), e.getMessageArgs(), e.getInterfaceType());

			// messageArgs 에 메시지를 넘기는 경우 처리
			if (e.getMessageArgs() != null && e.getExceptionCode().equals(e.getMessage())
					&& e.getMessageArgs().length > 0) {
				message = String.join(",", e.getMessageArgs());
			}
		}
		
		ErrorVo errorVo = new ErrorVo(code, message);
		return ResponseBase.error(errorVo);
	}

	@ExceptionHandler(value = Exception.class)
	public @ResponseBody ResponseEntity<?> defaultErrorHandler(Exception e) throws Exception {

		log.error(e.getMessage(), e);

		return ResponseBase.error(getDefaultErrorVo(e));
	}
	
	private ErrorVo getDefaultErrorVo(Exception e) {
		String code     = "ERROR.SYS.9999";
		String message  = MessageUtil.get(code, e.toString(), e.getMessage());
		ErrorVo errorVo = new ErrorVo(code, message);

		log.error(e.getMessage(), e);

		return errorVo;
	}
}
