package com.skt.twd.core.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.skt.twd.core.filter.wrapper.RequestWrapper;
import com.skt.twd.core.logging.TSSLoggingAspect;

/**
 * <ul>
 * <li>업무명 : HttpRequestWrapperFilter </li>
 * <li>설  명 : HttpRequestWrapperFilter.</li>
 * <li>작성일 : 2018. 4. 1.</li>
 * <li>작성자 : SKCC</li>
 * </ul>
 */
public class HttpRequestWrapperFilter implements Filter {
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
		// Instance (Application) 종료 시점에 TSS AOP logging 수행 ThreadLocal 변수 삭제
		TSSLoggingAspect.instance.remove(); 	
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		chain.doFilter(new RequestWrapper((HttpServletRequest) request), response);

		// Request 종료 시점에 ThreadLocal 변수 내 TSS AOP Logging 수행 기록 초기화
		TSSLoggingAspect.instance.set("false");

	}
}