package com.skt.twd.core.filter.wrapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.springframework.util.StreamUtils;

/**
* <ul>
* <li>업무명 : RequestWrapper </li>
* <li>설  명 : request Wrapper </li>
* <li>작성일 : 2018. 4. 1.</li>
* <li>작성자 : SKCC</li>
* </ul>
*/
public final class RequestWrapper extends HttpServletRequestWrapper {
	private byte[] rawData;

	public RequestWrapper(HttpServletRequest servletRequest) throws IOException {
		super(servletRequest);

		try {
			InputStream inputStream = servletRequest.getInputStream();
			this.rawData = StreamUtils.copyToByteArray(inputStream);
		} catch (IOException e) {
			throw e;
		}
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.rawData);
		
		ServletInputStream servletInputStream = new ServletInputStream() {
			public int read() throws IOException {
				return byteArrayInputStream.read();
			}

			@Override
			public boolean isFinished() {
				return false;
			}

			@Override
			public boolean isReady() {
				return false;
			}

			@Override
			public void setReadListener(ReadListener listener) {
			}
		};
		return servletInputStream;
	}
}