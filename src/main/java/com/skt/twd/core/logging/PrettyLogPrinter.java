package com.skt.twd.core.logging;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

/**
 * Created by alcava00 on 2018. 4. 18..
 */
public interface PrettyLogPrinter<T> {

	void printTitle(String title, T writer);

	void printSubTitle(String title, T writer);

	void printKeyValue(String key, Object value, T writer);

	void printSubKeyValue(String key, Object value, T writer);

	void printKeyValues(String key, Object[] value, T writer);

	void printString(String value, T writer);

	void printHorizon(String paddingValue, T writer);

	void flush(T writer);

	int getLineSize();

	public static class DefaultPrettyLogPrinter implements PrettyLogPrinter<Logger> {

		@Override
		public void printTitle(final String title, final Logger writer) {
			if (writer.isDebugEnabled()) {
				writer.debug(title(title));
			}
		}

		protected String title(final String title) {
			return StringUtils.center(" " + title + " ", getLineSize(), "*");
		}

		@Override
		public void printSubTitle(final String title, final Logger writer) {
			if (writer.isDebugEnabled()) {
				writer.debug(subTitle(title));
			}
		}

		protected String subTitle(final String title) {
			return StringUtils.center(" " + title + " ", getLineSize(), "*");
		}

		@Override
		public void printKeyValue(final String key, final Object value, final Logger writer) {
			if (writer.isDebugEnabled()) {
				writer.debug(keyValue(key, value));
			}
		}

		@Override
		public void printSubKeyValue(final String key, final Object value, final Logger writer) {
			if (writer.isDebugEnabled()) {
				writer.debug(subKeyValue(key, value));
			}
		}

		protected String keyValue(final String key, final Object value) {
			return String.format("*** %-25s : %s", key, value);
		}

		protected String subKeyValue(final String key, final Object value) {
			return String.format("*** %-25s : %s", key, value);
		}

		@Override
		public void printKeyValues(final String key, final Object[] values, final Logger writer) {
			if (writer.isDebugEnabled()) {
				if (values != null && values.length > 0) {
					writer.debug(String.format("*** %-25s :", key));
					for (Object value : values) {
						writer.debug(String.format("    %s :", value));
					}
				} else {
					writer.debug(String.format("*** %-25s : null ", key));
				}
			}
		}

		@Override
		public void printString(final String value, final Logger writer) {
			writer.info(value);
		}

		@Override
		public void printHorizon(final String paddingValue, final Logger writer) {
			writer.info(StringUtils.center(paddingValue, getLineSize(), paddingValue));
		}

		@Override
		public void flush(final Logger writer) {

		}

		@Override
		public int getLineSize() {
			return 80;
		}
	}
}
