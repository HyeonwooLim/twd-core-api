package com.skt.twd.core.logging;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TSSExceptionLog extends TSSLog {

	private static final long serialVersionUID = 6003535228562493405L;

	// Error 발생 시 Session Attribute 값
	// - SESSION 정보에 포함된 항목 (평문형태)
	// - 모바일 Tworld의 경우, 회원/다회선 등에 대한 정보와 고객정보 포함 항목이 존재하여, 보안 검토 필요 
	// - 필요 항목에 대해서 재 정의 필요
	@JsonProperty("EXP_SESSION_PARAM")
	private Map<String, Object> expSessionParam;

	// Error 발생 시 method parameter정보 - 암호문형태
	@JsonProperty("EXP_METHOD_INPUT_PARAM")
	private String expMethodInputParam;

	// 외부 I/F 대상 시스템 - I/F 에러 시, 수집 항목 
	// - Swing (MCG) : SWING
	// - MQ/EAI : EAI
	// - ICAS : ICAS
	// - TID : TID
	// - SMS/MMS : SMS
	// - IMAS : IMAS
	// - 검색서버 (Talkro) : SEARCH
	// - SKTelink : SKTEL
	// - 우리은행 (에스크로) : BANK
	// - KCP : KCP
	// - SNS (카카오톡) : SNS
	// - 우체국택배 : POST
	// - T멤버십 (R2K) : MBR
	// - 한신정 (구_Tgate I/F) : TGATE
	// - 이니텍 (구_Tgate I/F) : TGATE
	// - 한신평 (구_Tgate I/F) : TGATE
	@JsonProperty("EXP_IF_TYPE")
	private String expIfType;

	// Error Stack Trace정보 - Exception Stack Trace 정보(평문)
	@JsonProperty("EXP_TRACE_DTL")
	private String expTraceDtl;

	// Exception class명
	// - BizException : MS 업무
	// - InterfaceClientException : I/F업무
	@JsonProperty("EXP_TYPE")
	private String expType;

	// Error 발생 Class 명
	@JsonProperty("EXP_LAST_API_CLASS")
	private String expLastApiClass;

	// Error 발생 Method 명
	@JsonProperty("EXP_LAST_API_METHOD")
	private String expLastApiMethod;

	// 채널 Error Code
	@JsonProperty("EXP_CODE")
	private String expCode;

	// 채널 Error 메시지1 - 사용자 에러 문구
	@JsonProperty("EXP_MSG1")
	private String expMsg1;

	// 채널 Error 메시지2 - 시스템 에러 문구
	@JsonProperty("EXP_MSG2")
	private String expMsg2;
	
	// 추가필드
	@JsonProperty("EXTRA_FIELD1")
	private String extraField1;
	
	// 추가필드
	@JsonProperty("EXTRA_FIELD2")
	private String extraField2;
	
	// 추가필드
	@JsonProperty("EXTRA_FIELD3")
	private String extraField3;
	
	// 추가필드
	@JsonProperty("EXTRA_FIELD4")
	private String extraField4;
	
	// 추가필드
	@JsonProperty("EXTRA_FIELD5")
	private String extraField5;
}
