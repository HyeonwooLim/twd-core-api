package com.skt.twd.core.logging;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TSSLog implements Serializable {

	protected static final long serialVersionUID = 4270364733832440135L;

	// 채널서비스구분 - 온라인 Tworld Direct : PTD, 모바일웹 Tworld Direct : WTD
	@JsonProperty("CHANNEL_ID")
	protected String channelId;

	// 서버 명 - PaaS : ICP Private IP
	@JsonProperty("HOST_NAME")
	protected String hostName;

	// ICP POD 명 - PaaS : ICP POD 명
	@JsonProperty("POD_NAME")
	protected String podName;

	// CONTAINER 명 - PaaS : (middleware) CONTAINER 명
	@JsonProperty("CONTAINER_NAME")
	protected String containerName;

	// 정상로그, Exception 로그 구분 - 정상코드 : SUCCESS, 비정상코드: FAIL
	@JsonProperty("LOG_TYPE")
	protected LogType logType;

	// 업무구분 - PaaS : (MSA core 기반) 업무명(twd-wireless-api, twd-order-api 등)
	@JsonProperty("BIZ_NAME")
	protected String bizName;
	
	// 요청 Scheme - HTTP/HTTPS (대문자)
	@JsonProperty("REQUEST_SCHEM")
	protected String requestSchem;

	// 요청 URI
	@JsonProperty("REQUEST_URI")
	protected String requestUri;
	
	// 요청 Domain - domain:port
	@JsonProperty("REQUEST_DOMAIN")
	protected String requestDomain;
	
	// client IP
	@JsonProperty("REQUEST_CLIENT_IP")
	protected String requestClientIp;
	
	// 요청 method
	@JsonProperty("REQUEST_METHOD")
	protected String requestMethod;
	
	// 요청 parameters
	@JsonProperty("REQUEST_PARAM")
	protected String requestParam;
	
	// 세션 Key 값 - PaaS : RedisSessionId
	@JsonProperty("SESSION_KEY")
	protected String sessionKey;

	// Exception 발생시의 TRACE_ID(거래추적번호) - PaaS : Request Trasaction ID
	@JsonProperty("TX_KEY")
	protected String txKey;

	// Xtractor 수집(Device 기준 정보) - Xtractor 생성정보(Hash값)
	@JsonProperty("V_ID")
	protected String xtractorVId;

	// Xtractor 수집(서비스관리번호) - Xtractor 생성정보(대표회선관리번호 : 암호화값)
	@JsonProperty("L_ID")
	protected String xtractorLId;

	// Xtractor 수집(로그인 ID) - Xtractor 생성정보(로그인 ID : 암호화값)
	@JsonProperty("LOGIN_ID")
	protected String loginId;

	// T-ID 사용자 정보
	// - 고객정보 보안 사항으로 Member channel ID 대체 수집
	// - T-ID 사용자 아이디 logging 부분
	// - 비로그인 사용자 null (평문형태)
	@JsonProperty("MBR_CHL_ID")
	protected String mbrChlId;

	// USER AGENT 정보(request 로 보낸 정보)
	@JsonProperty("USER_AGENT")
	protected String userAgent;

	// 로그 발생 시간 - YYYYMMDD24hhmissff3
	@JsonProperty("LOG_TIME_STAMP")
	protected String logTimeStamp;

	// 요청 처리 시간(s) - (주요업무 정의 후) 단계 별 Response 응답 성공 시 기재
	@JsonProperty("REQUEST_PROCESS_TIME")
	protected String requestProcessTime;

	public enum LogType {
		SUCCESS, FAIL;
	}
}
