package com.skt.twd.core.logging;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.ContentCachingRequestWrapper;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import com.skt.twd.cache.CacheConstants;
import com.skt.twd.cache.CacheConstants.REDIS_CACHE;
import com.skt.twd.cache.common.utils.CacheUtils;
import com.skt.twd.cache.model.TssMeta;
import com.skt.twd.core.exceptions.BaseException;
import com.skt.twd.core.exceptions.InterfaceClientException;
import com.skt.twd.core.exceptions.InterfaceClientException.InterfaceType;
import com.skt.twd.core.filter.wrapper.RequestWrapper;
import com.skt.twd.core.logging.TSSLog.LogType;
import com.skt.twd.core.mvc.restclient.ClientRequestInterceptor;
import com.skt.twd.core.utils.MessageUtil;
import com.skt.twd.core.utils.RuntimeEnvironmentInfoUtil;

import brave.Tracer;
import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class TSSLoggingAspect {

	// 기본 에러 코드
	private static final String DEFAULT_SYS_ERROR_CODE = "ERROR.SYS.9999";
	// 콘솔 출력용 로거명
	private static final String LOGGER_NAME_TRACE_LOG = "errorTrace";
	// TSS(센싱) 출력용 로거명
	private static final String LOGGER_NAME_INTEGRATION_LOG = "integrationLog";
	// 콘솔 에러 trace 로그
	private static final Logger errorTraceLog = LoggerFactory.getLogger(LOGGER_NAME_TRACE_LOG);
	// TSS(센싱) 로그
	private static final Logger tssErrorLog = LoggerFactory.getLogger(LOGGER_NAME_INTEGRATION_LOG);

	// 채널명
	private static final String CHANNEL_NAME_WEB = "twd-bff-web";
	private static final String CHANNEL_NAME_MOBILE = "twd-bff-mobile";
	private static final String CHANNEL_NAME_OPENCHANNEL = "twd-bff-openchannel";
	private static final String CHANNEL_NAME_ADMIN = "twd-admin";

	// client ip 조회용 header key
	private static final String HTTP_HEADER_KEY_X_FORWARDED_FOR= "x-forwarded-for";

	// 파라미터 암호화 키
	private static final String secretKey = "TSS-TD-7551e058f";

	private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");

	@SuppressWarnings("serial")
	private final Map<String, String> channelMap = Collections.unmodifiableMap(new HashMap<String, String>() {
		{
			put(CHANNEL_NAME_WEB, "PTD");
			put(CHANNEL_NAME_MOBILE, "WTD");
			put(CHANNEL_NAME_OPENCHANNEL, "ETD");
			put(CHANNEL_NAME_ADMIN, "CTD");
		}
	});

	@Autowired(required = false)
	private Tracer tracer;

	// Throwing 중복 에러 발생 방지를 위한 Thread 변수 선언 (변수명 변경 가능)
	public static ThreadLocal<String> instance = new ThreadLocal<>();

	@Pointcut("execution(* com.skt..*(..))"
			+ "&& !execution(* com.skt.twd.core.logging..*(..))")
	public void tssErrorPointCut() {
	}

	// INFO : 전체 Application 지정을 위한 최상위 경로 지정
	@AfterThrowing(pointcut = "tssErrorPointCut()", throwing = "ex")
	public void afterThrowingTSS(JoinPoint joinPoint, Throwable ex) {

		// Request Thread 내 AOP Logging 수행 여부 확인 (Request Thread 내 최초 1번만 Logging 수행)
		if (instance != null && instance.get() != null && "true".equals(instance.get().toString())) {
			return;
		}
		
		TssMeta tssMeta = CacheUtils.getCacheData(CacheConstants.REDIS_CACHE.TssMeta,
				CacheConstants.REDIS_CACHE.TssMeta.name().toLowerCase(), TssMeta.class);
		boolean isLogging = (tssMeta != null && !"Y".equals(tssMeta.getCreateLogYn())) ? false : true;
		if (!isLogging) {
			return;
		}
		
		try {

			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			HttpSession session = request.getSession(false);

			TSSExceptionLog tssExceptionLog = new TSSExceptionLog();

			tssLogBase(request, session, tssExceptionLog, LogType.FAIL);

			//BFF 에서만 확인 가능
			if (session != null) {
				Map<String, Object> expSessionParam = getExpSessionParam(session);
				tssExceptionLog.setExpSessionParam(expSessionParam);
			}

			tssExceptionLog.setExpMethodInputParam(getExpInputParam(joinPoint));

			if (ex instanceof InterfaceClientException) {
				InterfaceType expIfType = ((InterfaceClientException) ex).getInterfaceType();
				String ifType = expIfType != null ? expIfType.name() : "UNKNOWN";
				tssExceptionLog.setExpIfType(ifType);
			}

			String expTraceDtl = ExceptionUtils.getStackTrace(Optional.ofNullable(ExceptionUtils.getRootCause(ex)).orElse(ex));
			tssExceptionLog.setExpTraceDtl(expTraceDtl);

			tssExceptionLog.setExpType(ex.getClass().getName());

			tssExceptionLog.setExpLastApiClass(joinPoint.getSignature().getDeclaringTypeName());
			tssExceptionLog.setExpLastApiMethod(joinPoint.getSignature().getName());

			String errorCode = getErrorCode(ex);
			tssExceptionLog.setExpCode(errorCode);

			String errorMessage = getErrorMessage(ex, errorCode);
			tssExceptionLog.setExpMsg1(errorMessage);

			// 채널 Error 메시지2 - 시스템 에러 문구
			String expMsg2 = getDebugMessage(ex, errorCode);
			tssExceptionLog.setExpMsg2(expMsg2);
			
			if (!"ZNGMC1119".equals(errorCode)) { // TWD개통 사전체크 예외 처리
				// TSS(센싱) 로그(파일)
				tssErrorLog.error(getObjectMapper().writeValueAsString(tssExceptionLog));

				// 콘솔 로그를 kibana 에서 확인하기 위해 공백과 개행 추가
				DefaultPrettyPrinter printer = new DefaultPrettyPrinter().withObjectIndenter(new DefaultIndenter("  ", "\n"));
				// object 를 json 문자열로 변환 후 마지막 } 앞에 공백 추가
				String errorLog = getObjectMapper().writer(printer).writeValueAsString(tssExceptionLog).replaceAll("[}]$", " $0");
				// 콘솔 에러 trace 로그
				errorTraceLog.error(errorLog);
			}
			
		} catch (Exception e) {
			log.error("error afterThrowing TSS : {}", e);
		}

		instance.set("true");
	}

	private void tssLogBase(HttpServletRequest request, HttpSession session, TSSLog tssLog, LogType logType) {

		String logTimeStamp = LocalDateTime.now().format(dateTimeFormatter);

		String channelId = channelMap.get(StringUtils.defaultString(request.getHeader(ClientRequestInterceptor.HTTP_HEADER_KEY_CHANNEL_NAME), CHANNEL_NAME_WEB));
		String hostName = request.getLocalAddr();
		String podName = RuntimeEnvironmentInfoUtil.getPodName();
		String containerName = RuntimeEnvironmentInfoUtil.getPodName();
		String bizName = getBizName(request.getRequestURI(), request.getMethod());
		String requestSchem = StringUtils.isEmpty(request.getScheme()) ? "" : request.getScheme().toUpperCase();
		String requestUri = request.getRequestURI();
		String requestDomain = request.getServerName() + ":" + request.getServerPort();
		String requestClientIp = request.getHeader(HTTP_HEADER_KEY_X_FORWARDED_FOR);
		String requestMethod = StringUtils.defaultString(request.getMethod(), HttpMethod.GET.name());
		String requestParam = getRequestParam(request);
		String sessionKey = "";
		String traceId = "";
//		String spanId = "";
		String vId = getCookieValue("XTVID", request.getCookies());
		String lId = getCookieValue("XTLID", request.getCookies());
		String loginId = getCookieValue("XTLOGINID", request.getCookies());
		String mbrChlId = request.getHeader(ClientRequestInterceptor.HTTP_HEADER_KEY_MBR_CHANNEL_ID);
		String userAgent = StringUtils.defaultString(request.getHeader(HttpHeaders.USER_AGENT), "");

		tssLog.setChannelId(channelId);
		tssLog.setHostName(hostName);
		tssLog.setPodName(podName);
		tssLog.setContainerName(containerName);
		tssLog.setLogType(logType);
		tssLog.setBizName(bizName);
		tssLog.setRequestSchem(requestSchem);
		tssLog.setRequestUri(requestUri);
		tssLog.setRequestDomain(requestDomain);
		tssLog.setRequestClientIp(requestClientIp);
		tssLog.setRequestMethod(requestMethod);
		tssLog.setRequestParam(requestParam);
		//BFF 에서만 확인 가능
		if (session != null) {
			sessionKey = session.getId();
			tssLog.setSessionKey(sessionKey);
		}

		if (tracer != null && tracer.currentSpan() != null) {
			traceId = tracer.currentSpan().context().traceIdString();
//			spanId = HexCodec.toLowerHex(tracer.currentSpan().context().spanId());
			tssLog.setTxKey(traceId);
		}

		tssLog.setXtractorVId(vId);
		tssLog.setXtractorLId(lId);
		tssLog.setLoginId(loginId);
		tssLog.setMbrChlId(mbrChlId);
		tssLog.setUserAgent(userAgent);
		tssLog.setLogTimeStamp(logTimeStamp);
	}

	@SuppressWarnings("unchecked")
	private String getBizName(String requestURI, String method) {
		String bizName = "";

		if (!StringUtils.isEmpty(requestURI)) {
			bizName = "UNKNOWN";

			String url = REDIS_CACHE.ApiMeta.name() + ":" + method.toUpperCase() + "|" + requestURI;

			Map<String, Object> apiMetas = (Map<String, Object>) CacheUtils.getCacheData(REDIS_CACHE.ApiMeta);

			// apimeta 의 key 값
			ArrayList<String> keys = new ArrayList<String>(apiMetas.keySet());
			
			// apimeta 정보 중 {loginId} 와 같은 pathVariable 사용으로 인해 해당 정보를 정규식으로 변환하여 조회한다.
			// key 값을 정규식으로 전환하면서 원본 키와 구분자(!)로 연결 후 역순 정렬
			String seperator = "!";
			List<String> regKeys = keys.stream()
				.map(key -> key.replaceAll("(\\{[^\\}]*\\})", "([^/]+)").replaceAll("\\|", "\\\\|") + seperator + key)
				.sorted(Comparator.reverseOrder())
				.collect(Collectors.toList());

			for (String regKey : regKeys) {
				String regex = regKey.split(seperator)[0];

				if (Pattern.matches(regex, url)) {
					bizName = ((Map<String, String>)apiMetas.get(regKey.split(seperator)[1])).get("apiName");
//					log.debug("### matching regex[{}], url[{}]", regex, url);
					break;
				}
			}
			log.debug("### url[{}], bizName[{}]", url, bizName);
		}

		return bizName;
	}

	private String getExpInputParam(JoinPoint joinPoint) {
		String encExpInputParam = "";
		Object[] args = joinPoint.getArgs();
		if (args != null && args.length > 0) {
			try {
				Map<String, Object> expInputParam = new LinkedHashMap<String, Object>();
				CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();
				String[] parameterNames = codeSignature.getParameterNames();

				for (int i = 0, len = args.length; i < len; i++) {
					String value = null;
					String name = parameterNames[i];
					if (args[i] != null) {
						if (args[i] instanceof Map || args[i] instanceof List<?> || args[i] instanceof String) {
							value = args[i].toString();
						} else {
							value = ToStringBuilder.reflectionToString(args[i], ToStringStyle.DEFAULT_STYLE);
						}
					}
					expInputParam.put(name, value);
				}

				encExpInputParam = encrypt(expInputParam.toString());
			} catch (Exception e) {
				log.error("error occured EXP_METHOD_INPUT_PARAM. : {}", e.toString());
			}
		}
		return encExpInputParam;
	}

	private String encrypt(String source) {
		String encrypted = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(secretKey.getBytes("UTF-8"), "AES"), new IvParameterSpec(secretKey.substring(0, 16).getBytes("UTF-8")));
			encrypted = new String(Base64.encodeBase64(cipher.doFinal(source.getBytes("UTF-8"))));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encrypted;
	}

	private String getCookieValue(String key, Cookie[] cookies) {
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie != null && key.equals(cookie.getName())) {
					return cookie.getValue() == null ? "": cookie.getValue();
				}
			}
		}
		return "";
	}

	private Map<String, Object> getExpSessionParam(HttpSession session) {
		Map<String, Object> sessionParams = new HashMap<>();
		Enumeration<String> attributes = session.getAttributeNames();
		while (attributes.hasMoreElements()) {
			String key = attributes.nextElement();
			Object value = session.getAttribute(key);

			sessionParams.put(key, value);
		}
		return sessionParams;
	}

	private String getDebugMessage(Throwable ex, String errorCode) {
		BaseException be = null;
		String debugMessage = null;
		String[] args = null;
		if (ex instanceof BaseException) {
			be = ((BaseException) ex);
			debugMessage = be.getDebugMessage();
			// 오류 메세지 처리
			if (be != null) {
				errorCode = be.getExceptionCode();
				args = be.getMessageArgs();
			}
		}
		if (debugMessage == null) {
			try {
				if (DEFAULT_SYS_ERROR_CODE.equals(errorCode)) {
					debugMessage = MessageUtil.get(DEFAULT_SYS_ERROR_CODE, ex.getClass().toString(), ex.getMessage());
				} else {
					debugMessage = MessageUtil.get(errorCode, args);
				}
			} catch (Exception e) {
				log.warn("not found to message. errorCode : {}, args : {}", errorCode, args);
			}
		}
		return debugMessage;
	}

	private String getErrorMessage(Throwable ex, String errorCode) {
		BaseException be = null;
		if (ex instanceof BaseException) {
			be = ((BaseException) ex);
		}

		// 오류 메세지 처리
		String[] args = null;
		if (be != null) {
			errorCode = be.getExceptionCode();
			args = be.getMessageArgs();
		}

		String errorMessage = null;
		try {
			if (DEFAULT_SYS_ERROR_CODE.equals(errorCode)) {
				errorMessage = MessageUtil.get(DEFAULT_SYS_ERROR_CODE, ex.getClass().toString(), ex.getMessage());
			} else {
				errorMessage = MessageUtil.get(errorCode, args);
			}
		} catch (Exception e) {
			log.warn("not found to message. errorCode : {}, args : {}", errorCode, args);
		}

		return errorMessage;
	}

	private String getErrorCode(Throwable ex) {
		String errorCode = DEFAULT_SYS_ERROR_CODE;

		if (ex instanceof BaseException) {
			errorCode = Optional.ofNullable(((BaseException) ex).getExceptionCode()).orElse(errorCode);
		}
		return errorCode;
	}

	private String getRequestParam(HttpServletRequest request) {
		// Input Params 설정
		if (HttpMethod.POST.name().equals(request.getMethod()) || HttpMethod.PUT.name().equals(request.getMethod())
				|| HttpMethod.PATCH.name().equals(request.getMethod()) || HttpMethod.DELETE.name().equals(request.getMethod())) {

			// requestBody 값
			if (request instanceof RequestWrapper) {
				RequestWrapper requestWrapper = (RequestWrapper) request;
				try {
					return encrypt(StreamUtils.copyToString(requestWrapper.getInputStream(), Charset.defaultCharset()));
				} catch (IOException e) {
				}
			} else if (request instanceof ContentCachingRequestWrapper) {
				ContentCachingRequestWrapper cachingRequest = (ContentCachingRequestWrapper) request;
				return encrypt(new String(cachingRequest.getContentAsByteArray(), Charset.defaultCharset()));
			}
		} else {
			// GET인 경우 Query 정보를 Param 정보로 설정
			return encrypt(request.getQueryString());
		}
		return "";
	}

	// json 생성시 null -> "" 사용
	private ObjectMapper getObjectMapper() {
		ObjectMapper om = new ObjectMapper();
		om.setSerializerProvider(new CustomSerializerProvider());
		return om;
	}

	class CustomSerializerProvider extends DefaultSerializerProvider {

		private static final long serialVersionUID = 4735970622413055745L;

		public CustomSerializerProvider() {
			super();
		}

		public CustomSerializerProvider(CustomSerializerProvider provider, SerializationConfig config,
				SerializerFactory jsf) {
			super(provider, config, jsf);
		}

		@Override
		public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
			return new CustomSerializerProvider(this, config, jsf);
		}

		@Override
		public JsonSerializer<Object> findNullValueSerializer(BeanProperty property) throws JsonMappingException {
			return new EmptyStringSerializer();
		}
	}

	class EmptyStringSerializer extends JsonSerializer<Object> {

		public EmptyStringSerializer() {
		}

		@Override
		public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString("");
		}
	}
}
