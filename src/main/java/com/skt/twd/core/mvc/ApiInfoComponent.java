package com.skt.twd.core.mvc;

import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.skt.twd.core.AppConfig;
import com.skt.twd.core.mvc.model.ApiInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ApiInfoComponent {

	@Autowired
	private AppConfig appConfig;

	@Autowired
	private RequestMappingHandlerMapping handlerMapping;

	public List<ApiInfo> getApiInfo(){

		if(appConfig.isProduction()) {
			return new ArrayList();
		}

		Set keys =  handlerMapping.getHandlerMethods().keySet();
		ApiInfo apiInfo = null;
		List<ApiInfo> list = new ArrayList<ApiInfo>();

		for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
			RequestMappingInfo requestMappingInfo = (RequestMappingInfo) iterator.next();

			HandlerMethod handlerMethod = (HandlerMethod) handlerMapping.getHandlerMethods().get(requestMappingInfo);
			Class<?> clazz = handlerMethod.getMethod().getDeclaringClass();

			if(clazz.getPackage().getName().startsWith(appConfig.getBasePackage())) {
				ApiOperation apiOperation = handlerMethod.getMethodAnnotation(ApiOperation.class);

				Set<String> patterns = requestMappingInfo.getPatternsCondition().getPatterns();
				Set<RequestMethod> requestMethod = requestMappingInfo.getMethodsCondition().getMethods();

				apiInfo = new ApiInfo();
				apiInfo.setAppName(appConfig.getAppName());
				apiInfo.setVersion(appConfig.getVersion());
				apiInfo.setPackageName(clazz.getPackage().getName());
				apiInfo.setClassFullName(clazz.getName());
				apiInfo.setClassSimpleName(clazz.getSimpleName());

				if(clazz.getAnnotation(Api.class) != null) {
					apiInfo.setApiTags(String.join(",", clazz.getAnnotation(Api.class).tags()));
				}

				if (apiOperation != null) {
					setApiInfo(apiInfo, apiOperation);
				}

				int modifier = handlerMethod.getMethod().getModifiers();
				String returnTypeName = handlerMethod.getMethod().getReturnType().getName();
				String methodName 	  = handlerMethod.getMethod().getName();

				List<String> parameterName  = new ArrayList<String>();
				for(Parameter p : handlerMethod.getMethod().getParameters()) {
					parameterName.add(p.getType().getName() + " " + p.getName());
				}

				apiInfo.setMethodName(Modifier.toString(modifier) + " " + returnTypeName+ " " + methodName + "(" + String.join(", " , parameterName) + ")");
				apiInfo.setApiUrl(String.join("", patterns));

				if(requestMethod != null && requestMethod.size() > 0) {
					apiInfo.setHttpMethodName(requestMethod.stream().findFirst().get().name());
				}

				list.add(apiInfo);
			}
		}

		if(log.isDebugEnabled()) {
			for(ApiInfo a : list) {
				log.debug("{}", a);
			}
		}

		return list;
	}

	private ApiInfo setApiInfo(ApiInfo source, ApiOperation apiOperation) {
		if(apiOperation == null
				|| StringUtils.isBlank(apiOperation.value())) {
			return source;
		}

		String values   = apiOperation.value();
		String notes    = apiOperation.notes();
		String[] arrStr = values.trim().split(",");

		String apiId 	= arrStr[0].replaceAll(" ", "").replaceFirst("API-ID:", "");
		String apiName 	= arrStr.length > 1 ? arrStr[1].trim() : "";

		source.setApiId(apiId);
		source.setApiName(apiName);
		source.setApiDesc(StringUtils.isNotBlank(notes) ? notes.trim() : apiName);

		return source;
	}
}
