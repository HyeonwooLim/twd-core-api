package com.skt.twd.core.mvc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SimpleTimeZone;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.skt.twd.cache.CacheConstants.REDIS_CACHE;
import com.skt.twd.cache.common.utils.CacheUtils;
import com.skt.twd.cache.model.ApiMeta;
import com.skt.twd.core.AppConfig;
import com.skt.twd.core.exceptions.BizException;
import com.skt.twd.core.utils.JsonUtil;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class BlockApiComponent {

	@Autowired
	private AppConfig appConfig;

	@Autowired
	private BlockApiRedisDao blockApiRedisDao;

	/**
	 * <ul>
	 * <li>업무명 : API 차단체크</li>
	 * <li>설 명 : 관리자가 등록한 차단정보에 해당되는지 확인한다.</li>
	 * <li>작성일 : 2018. 9. 19.</li>
	 * <li>작성자 : P127580</li>
	 * <li>
	 * @param httpMethod
	 * @param url
	 * </li>
	 * <li>
	 * @throws BizException
	 * </li>
	 * </ul>
	 */
	public void check(HttpMethod httpMethod, String url) throws BizException {
		ApiMeta apiMeta = blockApiRedisDao.getApiMeta(httpMethod.name() + "|" + url);
		String[] args = { url };

		if (apiMeta != null) {
			if ("Y".equals(apiMeta.getUseYn())
					&& "Y".equals(apiMeta.getIsolYn())) {
				log.debug("진입URL [{}] : 금일차단, Type : Y/N", url);
				throw new BizException("ERROR.COMM.0001", args);
			}
			checkUrl(httpMethod, url, apiMeta);
			checkGroup(httpMethod, url, apiMeta);
			checkLegacy(httpMethod, url, apiMeta);
		} else {
			throw new BizException("ERROR.COMM.0003", args);
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : Check Url</li>
	 * <li>설 명 : ApiMeta 등록된 URL 정보의 차단여부를 확인한다.</li>
	 * <li>작성일 : 2018. 9. 19.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param httpMethod
	 * @param url
	 * @param apiMeta
	 * </li>
	 * <li>
	 * @throws BizException
	 * </li>
	 * </ul>
	 */
	private void checkUrl(HttpMethod httpMethod, String url, ApiMeta apiMeta) {
		checkApiMeta("Url", httpMethod, url, apiMeta, apiMeta.getApiIsolStaDtm(), apiMeta.getApiIsolEndDtm());
	}

	/**
	 * <ul>
	 * <li>업무명 : Check Group</li>
	 * <li>설 명 : ApiMeta 등록된 Group 정보의 차단여부를 확인한다.</li>
	 * <li>작성일 : 2018. 9. 19.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param httpMethod
	 * @param url
	 * @param apiMeta
	 * </li>
	 * <li>
	 * @throws BizException
	 * </li>
	 * </ul>
	 */
	private void checkGroup(HttpMethod httpMethod, String url, ApiMeta apiMeta) {

		List<String> stList = apiMeta.getGrpIsolStaDtm();
		List<String> edList = apiMeta.getGrpIsolEndDtm();

		if (stList != null && edList != null) {
			int i = 0;
			int size = stList.size();

			for (i = 0; i < size; i++) {
				checkApiMeta("Group", httpMethod, url, apiMeta, stList.get(i), edList.get(i));
			}
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : Check Legacy</li>
	 * <li>설 명 : ApiMeta 등록된 Legacy 정보의 차단여부를 확인한다.</li>
	 * <li>작성일 : 2018. 9. 19.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param httpMethod
	 * @param url
	 * @param apiMeta
	 * </li>
	 * <li>
	 * @throws BizException
	 * </li>
	 * </ul>
	 */
	private void checkLegacy(HttpMethod httpMethod, String url, ApiMeta apiMeta) {
		List<String> stList = apiMeta.getLegIsolStaDtm();
		List<String> edList = apiMeta.getLegIsolEndDtm();

		if (stList != null && edList != null) {
			int i = 0;
			int size = stList.size();

			for (i = 0; i < size; i++) {
				checkApiMeta("Legacy", httpMethod, url, apiMeta, stList.get(i), edList.get(i));
			}
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : Check ApiMeta</li>
	 * <li>설 명 : ApiMeta 등록된 차단정보의 차단여부를 확인한다.</li>
	 * <li>작성일 : 2018. 9. 19.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param type
	 * @param httpMethod
	 * @param url
	 * @param apiMeta
	 * @param startDtm
	 * @param endDtm
	 * </li>
	 * <li>
	 * @throws BizException
	 * </li>
	 * </ul>
	 */
	private void checkApiMeta(String type, HttpMethod httpMethod, String url, ApiMeta apiMeta, String startDtm, String endDtm) {
		long nowDate = Long.parseLong(getCurrentTime());
		long startBlockTime = 0;
		long endBlockTime = 0;

		String targetUrl 		= apiMeta.getApiUrl();
		String targetHttpMethod = apiMeta.getHttpMethod();

		if (StringUtils.isNotEmpty(startDtm) && StringUtils.isNotEmpty(endDtm)) {
			startBlockTime = Long.parseLong(startDtm);
			endBlockTime = Long.parseLong(endDtm);

			boolean checkUrl = url.equals(targetUrl) && httpMethod == HttpMethod.valueOf(targetHttpMethod.toUpperCase()) ;
			boolean checkTime = (startBlockTime <= nowDate && endBlockTime > nowDate);
			boolean isBlock = checkUrl && checkTime;

			if (isBlock) {
				log.debug("진입URL [{}] : 금일차단, Type : {}", url, type);
				String[] args = { url, String.valueOf(startBlockTime), String.valueOf(endBlockTime) };
				throw new BizException("ERROR.COMM.0002", args);
			}
		}
	}

	private String getCurrentTime() {
		String DATE_FORMAT = "yyyyMMddHHmm"; // "yyyyMMddHHmmss"
		final int millisPerHour = 60 * 60 * 1000;

		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		SimpleTimeZone timeZone = new SimpleTimeZone(9 * millisPerHour, "KST");
		sdf.setTimeZone(timeZone);

		long time = System.currentTimeMillis();
		java.util.Date date = new java.util.Date(time);

		return sdf.format(date);
	}

	@Component
	class BlockApiRedisDao {
		/**
		 * <ul>
		 * <li>업무명 : API 차단목록</li>
		 * <li>설 명 : 관리자가 등록한 차단정보 목록을 조회한다.</li>
		 * <li>작성일 : 2018. 9. 19.</li>
		 * <li>작성자 : P127602</li>
		 * <li>
		 * @return List<ApiMeta>
		 * </li>
		 * </ul>
		 */
		public List<ApiMeta> getApiMetaList() {
			Map map = (Map) CacheUtils.getCacheData(REDIS_CACHE.ApiMeta);
			log.debug("CacheUtils.getCacheData(REDIS_CACHE.ChannelMeta) : {}", map);

			ApiMeta apiMeta = null;
			List<ApiMeta> list = new ArrayList<ApiMeta>();

			Set key = map.keySet();
			String keyName = "";

			for (Iterator iterator = key.iterator(); iterator.hasNext();) {
				keyName = (String) iterator.next();
//				apiMeta = (ApiMeta) map.get(keyName);

				try {
        			apiMeta = JsonUtil.convertValue(map.get(keyName), ApiMeta.class);
        		}catch(Exception e) {
        			log.error(e.getMessage(), e);
        			return null;
        		}

				list.add(apiMeta);
			}
			return list;
		}

		/**
		 * <ul>
		 * <li>업무명 : API 차단정보</li>
		 * <li>설 명 : 관리자가 등록한 차단정보를 조회한다.</li>
		 * <li>작성일 : 2018. 9. 19.</li>
		 * <li>작성자 : P127602</li>
		 * <li>
		 * @param url
		 * </li>
		 * <li>
		 * @return ApiMeta
		 * </li>
		 * </ul>
		 */
		public ApiMeta getApiMeta(String url) {
			return CacheUtils.getCacheData(REDIS_CACHE.ApiMeta, url, ApiMeta.class);
		}
	}
}
