package com.skt.twd.core.mvc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import com.skt.twd.core.utils.ObjectConvertUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings("unchecked")
public class ObjectWrapper {
	private Object sourceObject;
	private String accessType = ObjectConvertUtil.ACCESS_TYPE_METHOD;

	public ObjectWrapper() {
	}

	public ObjectWrapper(Object sourceObject) {
		this.sourceObject = sourceObject;
	}

	public ObjectWrapper(Object sourceObject, String accessType) {
		this.sourceObject = sourceObject;
		this.accessType = accessType;
	}

	public <T> T get() {
		return (T) this.sourceObject;
	}

	public void set(Object sourceObject) {
		this.sourceObject = sourceObject;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public void applyAccessTypeField() {
		this.accessType = ObjectConvertUtil.ACCESS_TYPE_FIELD;
	}

	public <T> T convertObject(Class<?> clazz) {
		return ObjectConvertUtil.toTargetObject(get(), clazz);
	}

	public <T> T convertChildObject(Class<?> clazz) {
		Object object = getChildObject(clazz.getSimpleName());
		if(object == null) {
			object = ObjectConvertUtil.newInstance(clazz);
		}
		return ObjectConvertUtil.toTargetObject(object, clazz);
	}

	public <T> T convertObjectDefaultValue(Class<?> clazz) {
		return ObjectConvertUtil.toTargetObjectDefaultValue(get(), clazz);
	}

	public <T> T convertChildObjectDefaultValue(Class<?> clazz) {
		Object object = getChildObject(clazz.getSimpleName());
		if(object == null) {
			object = ObjectConvertUtil.newInstance(clazz);
		}
		return ObjectConvertUtil.toTargetObjectDefaultValue(object, clazz);
	}

	public HashMap<String, Object> toHashMap() {
		return ObjectConvertUtil.convertObjectToHashMap(get(), accessType);
	}

	public HashMap<String, Object> toHashMapChildObject(Class<?> clazz) {
		Object object = getChildObject(clazz.getSimpleName());
		return ObjectConvertUtil.convertObjectToHashMap(object, accessType);
	}

	public HashMap<String, Object> toHashMapUpperUnderscore() {
		return ObjectConvertUtil.convertObjectToHashMapUpperUnderscore(get(), accessType);
	}

	public HashMap<String, Object> toHashMapUpperUnderscore(Class<?> clazz) {
		Object object = getChildObject(clazz.getSimpleName());
		return ObjectConvertUtil.convertObjectToHashMapUpperUnderscore(object, accessType);
	}

	public HashMap<String, Object> toHashMapLowerUnderscore() {
		return ObjectConvertUtil.convertObjectToHashMapLowerUnderscore(get(), accessType);
	}

	public HashMap<String, Object> toHashMapLowerUnderscore(Class<?> clazz) {
		Object object = getChildObject(clazz.getSimpleName());
		return ObjectConvertUtil.convertObjectToHashMapLowerUnderscore(object, accessType);
	}

	public Map<String, Object> toMap() {
		return ObjectConvertUtil.convertObjectToMap(get(), accessType);
	}

	public Map<String, Object> toMapChildObject(Class<?> clazz) {
		Object object = getChildObject(clazz.getSimpleName());
		return ObjectConvertUtil.convertObjectToMap(object, accessType);
	}

	public Map<String, Object> toMapUpperUnderscore() {
		return ObjectConvertUtil.convertObjectToMapUpperUnderscore(get(), accessType);
	}

	public Map<String, Object> toMapUpperUnderscore(Class<?> clazz) {
		Object object = getChildObject(clazz.getSimpleName());
		return ObjectConvertUtil.convertObjectToMapUpperUnderscore(object, accessType);
	}

	public Map<String, Object> toMapLowerUnderscore() {
		return ObjectConvertUtil.convertObjectToMapLowerUnderscore(get(), accessType);
	}

	public Map<String, Object> toMapLowerUnderscore(Class<?> clazz) {
		Object object = getChildObject(clazz.getSimpleName());
		return ObjectConvertUtil.convertObjectToMapLowerUnderscore(object, accessType);
	}

	private Object getChildObject(String classSimpleName) {
		return getMethodInvoke(classSimpleName);
	}

	private <T> T getMethodInvoke(String key) {
		String methodName = "get" + key.substring(0, 1).toUpperCase() + key.substring(1);
		Method method = BeanUtils.findMethod(get().getClass(), methodName);
		Object value = null;

		try {
			value = method.invoke(get());
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			log.error(e.getMessage(), e);
		}

		return (T) value;
	}

}