package com.skt.twd.core.mvc;

public interface ValidMarker {
	interface Create{};
	interface Select{};
	interface Update{};
	interface Delete{};
}
