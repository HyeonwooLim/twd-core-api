package com.skt.twd.core.mvc.convert;

import java.util.Date;

import com.fasterxml.jackson.databind.module.SimpleModule;

public class DateTimeModule extends SimpleModule {
    public DateTimeModule() {
        super();
        // 모듈에 Serializer를 등록해 준다.
        addSerializer(Date.class, new DateTimeSerializer());
    }
}

