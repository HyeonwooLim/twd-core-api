package com.skt.twd.core.mvc.convert;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DateTimeSerializer extends JsonSerializer<Date> {
    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        // 원하는 timestamp 필드만을 serialize 한다.
        gen.writeNumber(value.getTime());
    }
}