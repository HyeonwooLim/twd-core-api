package com.skt.twd.core.mvc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.skt.twd.core.AppConfig;
import com.skt.twd.core.mvc.BlockApiComponent;

import lombok.extern.slf4j.Slf4j;

/**
* <ul>
* <li>업무명 : Block Api Interceptor </li>
* <li>설  명 : 요청을 가로채어 API 차단 여부를 확인한다.</li>
* <li>작성일 : 2018. 9. 4.</li>
* <li>작성자 : P127602 </li>
* </ul>
*/
@Component
@Slf4j
public class BlockApiInterceptor extends HandlerInterceptorAdapter {

	/**
	 * appConfig
	 */
	@Autowired
	private AppConfig appConfig;

	/**
	 * blockApi
	 */
	@Autowired
	private BlockApiComponent blockApiComponent;

	@Override
	public boolean preHandle( HttpServletRequest request,
								HttpServletResponse response,
								Object handler ) throws Exception {
		// HTTP 요청 처리 전 수행할 로직 작성
//		blockApiComponent.check(HttpMethod.valueOf(request.getMethod()), request.getRequestURI());

		return super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle( HttpServletRequest request,
							HttpServletResponse response, Object handler,
							ModelAndView modelAndView ) throws Exception {
		// HTTP 요청 처리 후 수행할 로직 작성
	}
}
