package com.skt.twd.core.mvc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel(description = "API 정보")
public class ApiInfo {

	@ApiModelProperty(name = "Application 명", value="config의 spring.application.name 정보", required=false)
	private String appName;

	@ApiModelProperty(name = "Version 명", value="config의 info.app.version 정보", required=false)
	private String version;

	@ApiModelProperty(name = "Package 명", value="controller class package 정보", required=false)
	private String packageName;

	@ApiModelProperty(name = "Class 명", value="class name 정보", required=false)
	private String classFullName;

	@ApiModelProperty(name = "Class 요약 명", value="class simple name 정보", required=false)
	private String classSimpleName;

	@ApiModelProperty(name = "API Tags", value="controller @API(tags='~') 정보", required=false)
	private String apiTags;

	@ApiModelProperty(name = "API ID", value="@ApiOperation(value = \"API-ID : API_APPS_APIINFO_001 , ...\", ...) 정보", required=true)
	private String apiId;

	@ApiModelProperty(name = "API 명", value="@ApiOperation(value = \"API-ID : ... , MS API정보 목록조회\", ...) 정보", required=true)
	private String apiName;

	@ApiModelProperty(name = "API 설명", value="@ApiOperation(value = \"API-ID : ... , ...\", notes=\"MS API정보 목록을 조회한다.\") 정보", notes="notes 값이 없는 경우  API 명으로 대체된다.", required=false)
	private String apiDesc;

	@ApiModelProperty(name = "Method 명", value="controller 실행 method 정보 ", required=false)
	private String methodName;

	@ApiModelProperty(name = "API URL", value="API URL 정보", required=true)
	private String apiUrl;

	@ApiModelProperty(name = "HTTP Method 명", value="요청된 API의 HTTP Method 명 (GET, POST, PUT, DELETE)", required=true)
	private String httpMethodName;
}
