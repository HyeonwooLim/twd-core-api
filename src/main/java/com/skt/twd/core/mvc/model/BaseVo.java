package com.skt.twd.core.mvc.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.skt.twd.core.exceptions.ErrorVo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BaseVo {
	@JsonProperty("error")
	private ErrorVo errorVo = new ErrorVo();
}
