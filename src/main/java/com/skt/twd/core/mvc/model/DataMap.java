package com.skt.twd.core.mvc.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.CaseFormat;

/**
 * <ul>
 * <li>업무명 : DataMap</li>
 * <li>설 명 : HashMap을 확장하여 Class 내부의 Data를 저장하거나 전달할때 사용.</li>
 * <li>작성일 : 2018. 05. 04.</li>
 * <li>작성자 : P127602</li>
 * </ul>
 */
public class DataMap<K, V> extends HashMap<K, V> {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6733847325267048834L;
	private boolean isCamelcase = true;

	public DataMap() {
	}
	
	public DataMap(boolean isCamelcase) {
		this.isCamelcase = isCamelcase;
	}

	/**
	 * <ul>
	 * <li>업무명 : DataMap(HashMap map)</li>
	 * <li>설 명 : 주어진 HashMap를 DataMap으로 변경하여 객체를 생성한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * </pre>
	 * </li>
	 * <li>
	 *
	 * @param map
	 * @return DataMap</li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataMap(HashMap map) {
		super();
		Set key = map.keySet();
		String keyName = "";
		String newKeyName = "";

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			keyName = (String) iterator.next();
			newKeyName = defaultStringConvert(keyName);
			this.put((K) newKeyName, (V) map.get(keyName));
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : DataMap(Map map)</li>
	 * <li>설 명 : 주어진 Map를 DataMap으로 변경하여 객체를 생성한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * </pre>
	 * </li>
	 * <li>
	 * @param map
	 * @return DataMap
	 * </li>
	 * </ul>
	 */
	public DataMap(Map map) {
		super();
		Set key = map.keySet();
		String keyName = "";
		String newKeyName = "";

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			keyName = (String) iterator.next();
			newKeyName = defaultStringConvert(keyName);
			this.put((K) newKeyName, (V) map.get(keyName));
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : DataMap(String totalPage, String currentPage, String totalCount,
	 * List resultList)</li>
	 * <li>설 명 : 주어진 목록 조회를 DataMap으로 변경하여 객체를 생성한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * </pre>
	 * </li>
	 * <li>
	 * @param totalPage
	 * @param currentPage
	 * @param totalCount
	 * @param resultList
	 * @return DataMap</li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataMap(String totalPage, String currentPage, String totalCount, List resultList) {
		super();
		put((K) defaultStringConvert("TOTAL_PAGE"), (V) totalPage);
		put((K) defaultStringConvert("CURRENT_PAGE"), (V) currentPage);
		put((K) defaultStringConvert("TOTAL_COUNT"), (V) totalCount);
		put((K) defaultStringConvert("RESULT_LIST"), (V) resultList);
	}
	
	public boolean isCamelcase() {
		return isCamelcase;
	}

	public void setCamelcase(boolean isCamelcase) {
		this.isCamelcase = isCamelcase;
	}

	@Override
	@SuppressWarnings("unchecked")
	public V put(K key, V value) {
		if (key instanceof String) {
			return super.put((K) defaultStringConvert((String) key), value);
		}
		return super.put(key, value);
	}

	@Override
	@SuppressWarnings("unchecked")
	public V get(Object key) {
		if (key instanceof String) {
			return super.get((K) defaultStringConvert((String) key));
		}
		return super.get(key);
	}

	/**
	 * <ul>
	 * <li>업무명 : defaultString(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열이 널(null)인경우 빈("") 문자열을 리턴하고 널 이외의 문자열은 그냥 리턴한다.
	 * </li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return String</li>
	 * </ul>
	 */
	public String defaultString(String key) {
		return defaultString(key, "");
	}

	/**
	 * <ul>
	 * <li>업무명 : defaultString(String key, String defValue)</li>
	 * <li>설 명 : 주어진 key의 value 문자열이 널(null)인경우, 주어진 기본문자열을 리턴하고 널 이외의 문자열은 그냥 리턴한다.
	 * </li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * </ul>
	 */
	public String defaultString(String key, String defValue) {
		String value = getString(key);
		return StringUtils.defaultString(value, defValue);
	}

	/**
	 * <ul>
	 * <li>업무명 : defaultIfEmpty(String key, String defValue)</li>
	 * <li>설 명 : 주어진 key의 value 문자열이 Empty인경우, 주어진 기본문자열을 리턴하고 널 이외의 문자열은 그냥 리턴한다.
	 * </li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * </ul>
	 */
	public String defaultIfEmpty(String key, String defValue) {
		return StringUtils.defaultIfEmpty(getString(key), defValue);
	}

	/**
	 * <ul>
	 * <li>업무명 : defaultIfNotEmpty(String key, String defValue1, String defValue2)
	 * </li>
	 * <li>설 명 : 주어진 key의 value 문자열이 NotEmpty인경우, defValue1 문자열을 리턴하고 아니면 defValue2
	 * 문자열을 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * </ul>
	 */
	public String defaultIfNotEmpty(String key, String defValue1, String defValue2) {
		return StringUtils.isNotEmpty(getString(key)) ? defValue1 : defValue2;
	}

	/**
	 * <ul>
	 * <li>업무명 : defaultInt(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열이 널(null)인경우, 기본 0을 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return int
	 * </li>
	 * </ul>
	 */
	public int defaultInt(String key) {
		return defaultInt(key, 0);
	}

	/**
	 * <ul>
	 * <li>업무명 : defaultInt(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열이 널(null)인경우, 주어진 기본값을 리턴하고 널 이외의 값은 int 로 변환하여
	 * 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * before : int phonePrice = Integer.parseInt("".equals(StringUtil.defaultString(paramMap.getString("PHONE_PRICE")))?"0":paramMap.getString("PHONE_PRICE"));
	 * after  : int phonePrice = datamMap.defaultInt("PHONE_PRICE", 10);
	 *
	 * datamMap.put("PHONE_PRICE", "200");
	 * int phonePrice = paramMap.defaultInt("PHONE_PRICE", 10); -> 200
	 *
	 * datamMap.put("PHONE_PRICE", ""); or datamMap.put("PHONE_PRICE", null);
	 * int phonePrice = paramMap.defaultInt("PHONE_PRICE", 10); -> 10
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @param defValue
	 * @return int</li>
	 * </ul>
	 */
	public int defaultInt(String key, int defValue) {
		String value = getString(key);
		return StringUtils.isEmpty(value) ? defValue : Integer.parseInt(value);
	}

	/**
	 * <ul>
	 * <li>업무명 : length(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열 length 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return int
	 * </li>
	 * </ul>
	 */
	public int length(String key) {
		return StringUtils.length(getString(key));
	}

	/**
	 * <ul>
	 * <li>업무명 : left(String key, int len)</li>
	 * <li>설 명 : 주어진 key의 value 문자열의 left 문자열을 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @param len
	 * @return String
	 * </li>
	 * </ul>
	 */
	public String left(String key, int len) {
		return StringUtils.left(getString(key), len);
	}

	/**
	 * <ul>
	 * <li>업무명 : leftPad(String key, int len)</li>
	 * <li>설 명 : 주어진 key의 value 문자열의 leftPad 문자열을 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @param len
	 * @return String
	 * </li>
	 * </ul>
	 */
	public String leftPad(String key, int len) {
		return StringUtils.leftPad(getString(key), len);
	}

	/**
	 * <ul>
	 * <li>업무명 : trimToEmpty(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열의 trimToEmpty 처리된 문자열을 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return String
	 * </li>
	 * </ul>
	 */
	public String trimToEmpty(String key) {
		return StringUtils.trimToEmpty(getString(key));
	}

	/**
	 * <ul>
	 * <li>업무명 : trim(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열의 trim처리된 문자열을 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return String
	 * </li>
	 * </ul>
	 */
	public String trim(String key) {
		return StringUtils.trim(getString(key));
	}

	/**
	 * <ul>
	 * <li>업무명 : getString(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열을 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return String
	 * </li>
	 * </ul>
	 */
	public String getString(String key) {
		if (key == null) {
			return "";
		}
		Object value = get(key);
		if (value == null) {
			return "";
		}
		return value.toString();
	}

	/**
	 * <ul>
	 * <li>업무명 : getSafeString(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열을 replaceCrossSiteScripting 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return String
	 * </li>
	 * </ul>
	 */
	public String getSafeString(String key) {
		return replaceCrossSiteScripting(getString(key));
	}

	/**
	 * <ul>
	 * <li>업무명 : getInt(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열을 int 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return int
	 * </li>
	 * </ul>
	 */
	public int getInt(String key) {
		if (key == null) {
			return 0;
		}
		Object value = get(key);
		if (value == null) {
			return 0;
		}
		if ((value instanceof String)) {
			try {
				return Integer.parseInt((String) value);
			} catch (NumberFormatException e) {
				return 0;
			}
		}
		if ((value instanceof Number)) {
			return ((Number) value).intValue();
		}
		return 0;
	}

	/**
	 * <ul>
	 * <li>업무명 : getLong(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열을 long 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return long
	 * </li>
	 * </ul>
	 */
	public long getLong(String key) {
		if (key == null) {
			return 0L;
		}
		Object value = get(key);
		if (value == null) {
			return 0L;
		}
		if ((value instanceof String)) {
			try {
				return Long.parseLong((String) value);
			} catch (NumberFormatException e) {
				return 0L;
			}
		}
		if ((value instanceof Number)) {
			return ((Number) value).longValue();
		}
		return 0L;
	}

	/**
	 * <ul>
	 * <li>업무명 : getDouble(String key)</li>
	 * <li>설 명 : 주어진 key의 value 문자열을 double 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return double
	 * </li>
	 * </ul>
	 */
	public double getDouble(String key) {
		if (key == null) {
			return 0.0D;
		}
		Object value = get(key);
		if (value == null) {
			return 0.0D;
		}
		if ((value instanceof String)) {
			try {
				return Double.parseDouble((String) value);
			} catch (NumberFormatException e) {
				return 0.0D;
			}
		}
		if ((value instanceof Number)) {
			return ((Number) value).doubleValue();
		}
		return 0.0D;
	}

	/**
	 * <ul>
	 * <li>업무명 : getList(String key)</li>
	 * <li>설 명 : 주어진 key의 value (["a","b","c"]) 을 List 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * example)
	 * - json data
	 * {
	 *   "SAMPLE1":[1,2,3,4,5],
	 *   "SAMPLE2":["a","b","c","d","e"],
	 *   "SAMPLE3":"a,b,c,d,e",
	 *   "SAMPLE4":"a-b-c-d-e",
	 *   "SAMPLE5":"1,2,3,4,5"
	 * }
	 *
	 * List<Integer> list1 = dataMap.getList("SAMPLE1");
	 * List<String>  list2 = dataMap.getList("SAMPLE2");
	 * List<String>  list3 = dataMap.getList("SAMPLE3");  //default split : ','
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return List
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> getList(String key) {
		try {
			if (super.get(defaultStringConvert(key)) instanceof Collection) {
				return (List<T>) super.get(defaultStringConvert(key));
			} else {
				return getList(defaultStringConvert(key), ",");
			}
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : getList(String key, String delim)</li>
	 * <li>설 명 : 주어진 key의 value ("a,b,c".split(delim)) 을 List 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * example)
	 * - json data
	 * {
	 *   "SAMPLE1":[1,2,3,4,5],
	 *   "SAMPLE2":["a","b","c","d","e"],
	 *   "SAMPLE3":"a,b,c,d,e",
	 *   "SAMPLE4":"a-b-c-d-e",
	 *   "SAMPLE5":"1,2,3,4,5"
	 * }
	 *
	 * List<String>  list4 = dataMap.getList("SAMPLE3", ",");
	 * List<String>  list5 = dataMap.getList("SAMPLE4", "-");
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return List
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> getList(String key, String delim) {
		try {
			return (List<T>) Arrays.asList(getArray(key, delim));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : getArray(String key)</li>
	 * <li>설 명 : 주어진 key의 value ("a,b,c".split(",")) 을 String[] 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * example)
	 * - json data
	 * {
	 *   "SAMPLE1":[1,2,3,4,5],
	 *   "SAMPLE2":["a","b","c","d","e"],
	 *   "SAMPLE3":"a,b,c,d,e",
	 *   "SAMPLE4":"a-b-c-d-e",
	 *   "SAMPLE5":"1,2,3,4,5"
	 * }
	 *
	 * String[] list6 = dataMap.getArray("SAMPLE3"); // default split : ','
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return String[]
	 * </li>
	 * </ul>
	 */
	public String[] getArray(String key) {
		try {
			return getArray(key, ",");
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : getArray(String key, String delim)</li>
	 * <li>설 명 : 주어진 key의 value ("a,b,c".split(delim)) 을 String[] 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * example)
	 * - json data
	 * {
	 *   "SAMPLE1":[1,2,3,4,5],
	 *   "SAMPLE2":["a","b","c","d","e"],
	 *   "SAMPLE3":"a,b,c,d,e",
	 *   "SAMPLE4":"a-b-c-d-e",
	 *   "SAMPLE5":"1,2,3,4,5"
	 * }
	 *
	 * String[] list7 = dataMap.getArray("SAMPLE3", ",");
	 * String[] list8 = dataMap.getArray("SAMPLE4", "-");
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return String[]
	 * </li>
	 * </ul>
	 */
	public String[] getArray(String key, String delim) {
		try {
			String str = (String) get(key);
			return str.split(delim);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : getIntArray(String key)</li>
	 * <li>설 명 : 주어진 key의 value ("1,2,3".split(",")) 을 int[] 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * example)
	 * - json data
	 * {
	 *   "SAMPLE1":[1,2,3,4,5],
	 *   "SAMPLE2":["a","b","c","d","e"],
	 *   "SAMPLE3":"a,b,c,d,e",
	 *   "SAMPLE4":"a-b-c-d-e",
	 *   "SAMPLE5":"1,2,3,4,5"
	 * }
	 *
	 * int[] list9 = dataMap.getIntArray("SAMPLE5"); // default split : ','
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return int[]
	 * </li>
	 * </ul>
	 */
	public int[] getIntArray(String key) {
		try {
			return getIntArray(key, ",");
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : getIntArray(String key, String delim)</li>
	 * <li>설 명 : 주어진 key의 value ("1,2,3".split(delim)) 을 int[] 처리하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * example)
	 * - json data
	 * {
	 *   "SAMPLE1":[1,2,3,4,5],
	 *   "SAMPLE2":["a","b","c","d","e"],
	 *   "SAMPLE3":"a,b,c,d,e",
	 *   "SAMPLE4":"a-b-c-d-e",
	 *   "SAMPLE5":"1,2,3,4,5"
	 * }
	 *
	 * int[] list10 = dataMap.getIntArray("SAMPLE5", ",");
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return int[]
	 * </li>
	 * </ul>
	 */
	public int[] getIntArray(String key, String delim) {
		try {
			String str = (String) get(key);
			String[] strArray = str.split(delim);

			int[] intArray = new int[strArray.length];
			int index = 0;
			for (String text : strArray) {
				intArray[index] = Integer.parseInt(text);
				index++;
			}

			return intArray;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : isEmpty(String key)</li>
	 * <li>설 명 : 주어진 key의 value 값 empty 확인</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * dataMap.isEmpty(null)      = true
	 * dataMap.isEmpty("")        = true
	 * dataMap.isEmpty(" ")       = false
	 * dataMap.isEmpty("bob")     = false
	 * dataMap.isEmpty("  bob  ") = false
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	public boolean isEmpty(String key) {
		return StringUtils.isEmpty(getString(key));
	}

	/**
	 * <ul>
	 * <li>업무명 : isNotEmpty(String key)</li>
	 * <li>설 명 : 주어진 key의 value 값 not empty 확인</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	public boolean isNotEmpty(String key) {
		return StringUtils.isNotEmpty(getString(key));
	}

	/**
	 * <ul>
	 * <li>업무명 : equals(String key, String text)</li>
	 * <li>설 명 : 주어진 key의 value 값과 text 값이 동일한지 비교</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param key
	 * @param text
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	public boolean equals(String key, String text) {
		return StringUtils.equals(getString(key), text);
	}

	/**
	 * <ul>
	 * <li>업무명 : isEmptyOr(List<String> keys)</li>
	 * <li>설 명 : 주어진 key들 중 하나라도 null(or "") 이면 true 아니면 false</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * before : if (StringUtil.isNull(sProductID) || StringUtil.isNull(sSerNum)) {}
	 * after  : if (dataMap.isEmptyOr(Arrays.asList("ID", "NUM"))) {}
	 *
	 * dataMap.put("ID", "AA")
	 * dataMap.put("NUM", "1")
	 * dataMap.isEmptyOr(Arrays.asList("ID", "NUM")) = false
	 *
	 * 위의 이외는 false
	 * dataMap.isEmptyOr(Arrays.asList("ID", "NUM")) = true
	 * </pre>
	 * </li>
	 * <li>
	 * @param keys
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	public boolean isEmptyOr(List<String> keys) {
		boolean result = false;
		for (String key : keys) {
			if (StringUtils.isEmpty(getString(key))) {
				result = true;
				break;
			}
		}

		return result;
	}

	/**
	 * <ul>
	 * <li>업무명 : isNotEmptyTrim(String key)</li>
	 * <li>설 명 : 주어진 key값을 trim 처리하여 isNotEmpty 이면 true 아니면 false</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * before : if(!"".equals(StringUtil.trim(map.getString("ADDR2")))) {}
	 * after  : if (dataMap.isNotEmptyTrim("ADDR2")) {}
	 *
	 * dataMap.put("ADDR2", " aaaa   ")
	 * dataMap.isNotEmptyTrim("ADDR2") = true
	 *
	 * dataMap.put("ADDR2", "   ") or dataMap.put("ADDR2", "")
	 * dataMap.isNotEmptyTrim("ADDR2") = false
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	public boolean isNotEmptyTrim(String key) {
		return StringUtils.isNotEmpty(StringUtils.trim(getString(key)));
	}

	/**
	 * <ul>
	 * <li>업무명 : isEqualsOr(String key, String valuesStr)</li>
	 * <li>설 명 : 주어진 key 값을 value 가 values 와 하나라도 equals 이면 true 아니면 false</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * before : if (StringUtils.equals("1", firstEntryCd) || StringUtils.equals("2", firstEntryCd) || StringUtils.equals("3", firstEntryCd)) {}
	 * after  : if(dataMap.isEqualsOr("FIRST_ENTRY_CD1", "a,b,c")){}
	 *
	 * dataMap.put("FIRST_ENTRY_CD1", "a")
	 * dataMap.isEqualsOr("FIRST_ENTRY_CD1", "a,b,c") -> true
	 *
	 * dataMap.put("FIRST_ENTRY_CD1", "a1") or dataMap.put("FIRST_ENTRY_CD1", "")
	 * dataMap.isEqualsOr("FIRST_ENTRY_CD1", "a,b,c") -> false
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @param valuesStr
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	public boolean isEqualsOr(String key, String valuesStr) {
		boolean result = false;
		String[] values = valuesStr.split(",");

		for (String value : values) {
			if (StringUtils.equals(getString(key), value)) {
				result = true;
				break;
			}
		}

		return result;
	}

	/**
	 * <ul>
	 * <li>업무명 : isEqualsIntOr(String key, String valuesStr)</li>
	 * <li>설 명 : 주어진 key 값을 value 가 valuesStr 와 하나라도 equals 이면 true 아니면 false</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * before : if (StringUtils.equals("1", firstEntryCd) || StringUtils.equals("2", firstEntryCd) || StringUtils.equals("3", firstEntryCd)) {}
	 * after  : if(dataMap.isEqualsIntOr("FIRST_ENTRY_CD2", "1,2,3")){}
	 *
	 * dataMap.put("FIRST_ENTRY_CD2", "a") or dataMap.put("FIRST_ENTRY_CD2", "")
	 * dataMap.isEqualsIntOr("FIRST_ENTRY_CD2", "1,2,3") -> false
	 *
	 * dataMap.put("FIRST_ENTRY_CD2", "1")
	 * dataMap.isEqualsIntOr("FIRST_ENTRY_CD2", "1,2,3") -> true
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @param valuesStr
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	public boolean isEqualsIntOr(String key, String valuesStr) {
		boolean result = false;
		String[] strArray = valuesStr.split(",");
		int[] values = new int[strArray.length];
		int index = 0;

		for (String str : strArray) {
			values[index] = Integer.parseInt(str.trim());
			index++;
		}

		for (int value : values) {
			if (getInt(key) == value) {
				result = true;
				break;
			}
		}

		return result;
	}

	/**
	 * <ul>
	 * <li>업무명 : isNotEqualsAll(String key, String valuesStr)</li>
	 * <li>설 명 : 주어진 key 값을 value 가 valuesStr 와 하나라도 equals 이면 false 아니면 true</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * before : if(!"01".equals(authCl) && !"02".equals(authCl) && !"03".equals(authCl) && !"04".equals(authCl) && !"05".equals(authCl)) {}
	 * after  : if(dataMap.isNotEqualsAll("FIRST_ENTRY_CD3", "a,b,c")){}
	 *
	 * dataMap.put("FIRST_ENTRY_CD3", "a")
	 * dataMap.isNotEqualsAll("FIRST_ENTRY_CD3", "a,b,c") -> false
	 *
	 * dataMap.put("FIRST_ENTRY_CD3", "1") or dataMap.put("FIRST_ENTRY_CD3", "")
	 * dataMap.isNotEqualsAll("FIRST_ENTRY_CD3", "a,b,c") -> true
	 * </pre>
	 * </li>
	 * <li>
	 *
	 * @param key
	 * @param valuesStr
	 * @return boolean
	 * </li>
	 *  </ul>
	 */
	public boolean isNotEqualsAll(String key, String valuesStr) {
		boolean result = true;
		String[] values = valuesStr.split(",");

		for (String value : values) {
			if (StringUtils.equals(getString(key), value)) {
				result = false;
				break;
			}
		}

		return result;
	}

	/**
	 * <ul>
	 * <li>업무명 : isNotEqualsIntAll(String key, String valuesStr)</li>
	 * <li>설 명 : 주어진 key 값을 value 가 values 와 하나라도 equals 이면 false 아니면 true</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * before : if(authCl != 1 && authCl != 2 && authCl != 3) {}
	 * after  : if(dataMap.isNotEqualsIntAll("FIRST_ENTRY_CD4", "1,2,3")){}
	 *
	 * dataMap.put("FIRST_ENTRY_CD4", "5")
	 * dataMap.isNotEqualsIntAll("FIRST_ENTRY_CD4", "1,2,3") -> false
	 *
	 * dataMap.put("FIRST_ENTRY_CD4", "1") or dataMap.put("FIRST_ENTRY_CD4", "")
	 * dataMap.isNotEqualsIntAll("FIRST_ENTRY_CD4", "1,2,3") -> true
	 * </pre>
	 * </li>
	 * <li>
	 * @param key
	 * @param valuesStr
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	public boolean isNotEqualsIntAll(String key, String valuesStr) {
		boolean result = true;
		String[] strArray = valuesStr.split(",");
		int[] values = new int[strArray.length];
		int index = 0;

		for (String str : strArray) {
			values[index] = Integer.parseInt(str.trim());
			index++;
		}

		for (int value : values) {
			if (getInt(key) == value) {
				result = false;
				break;
			}
		}

		return result;
	}

	/**
	 * <ul>
	 * <li>업무명 : replaceCrossSiteScripting(String value)</li>
	 * <li>설 명 : 주어진 value를 cross site scripting 변환한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param value
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	private String replaceCrossSiteScripting(String value) {
		if (value == null) {
			return value;
		}
		return value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

	/**
	 * <ul>
	 * <li>업무명 : Convert JsonKeyUpperUnderscore</li>
	 * <li>설 명 : Convert JsonKeyUpperUnderscore</li>
	 * <li>
	 * <pre>
	 * example)
	 *     {"queryPageNo":"1","queryEndTime":"20070405235959","queryStartTime":"20070405000000"}
	 *     -> {"QUERY_PAGE_NO":"1","QUERY_END_TIME":"20070405235959", "QUERY_START_TIME":"20070405000000"}
	 *
	 *     {"queryPageNo":"1","QUERY_end_TIME":"20070405235959", "QUERY_START_TIME":"20070405000000"}
	 *     -> {"QUERY_PAGE_NO":"1","QUERY_END_TIME":"20070405235959", "QUERY_START_TIME":"20070405000000"}
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 05. 21.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @return String
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes" })
	public String convertJsonKeyUpperUnderscore() {
		Set key = this.keySet();
		String keyName = "";
		String newKeyName = "";
		StringBuffer str = new StringBuffer("{");

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			keyName = (String) iterator.next();
			newKeyName = defaultStringConvert(keyName);

			str.append("\"").append(newKeyName).append("\"").append(":").append("\"").append(this.get(keyName))
					.append("\"");

			if (iterator.hasNext()) {
				str.append(",");
			}
		}

		str.append("}");

		return str.toString();
	}

	/**
	 * <ul>
	 * <li>업무명 : Default UpperUnderscore</li>
	 * <li>설 명 : Default UpperUnderscore</li>
	 * <li>
	 * <pre>
	 * example)
	 *     aaaBbbCcc   -> AAA_BBB_CCC
	 *     AAA_BBB_CCC -> AAA_BBB_CCC
	 *     AAA_bbb_CCC -> AAA_BBB_CCC
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 05. 21.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param text
	 * @return String
	 * </li>
	 * </ul>
	 */
	protected String defaultUpperUnderscore(String text) {
		String newText = "";

		if (!isAlpaUpperNum(text) && isAlpaNum(text)) {
			newText = convertUnderscoreOrCamel(text);
		} else {				
			newText = text.toUpperCase();
		}

		return newText;
	}
	
	
	/**
	 * <ul>
	 * <li>업무명 : Default Camelcase</li>
	 * <li>설 명 : Default Camelcase</li>
	 * <li>
	 * <pre>
	 * example)
	 *     aaaBbbCcc   -> aaaBbbCcc
	 *     AAA_BBB_CCC -> aaaBbbCcc
	 *     AAA_bbb_CCC -> aaaBbbCcc
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 08. 27.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param text
	 * @return String
	 * </li>
	 * </ul>
	 */
	protected String defaulCamelcase(String text) {
		String newText = "";
		
		if (!isAlpaUpperNum(text) && isAlpaNum(text)) {
			newText = text;
		} else {
			newText = convertCamelcase(text);
		}
		
		return newText;
	}
	
	/**
	 * <ul>
	 * <li>업무명 : Default StringConvert</li>
	 * <li>설 명 : Default StringConvert</li>
	 * <li>
	 * <pre>
	 * example 1) isCamelcase == true
	 *     aaaBbbCcc   -> aaaBbbCcc
	 *     AAA_BBB_CCC -> aaaBbbCcc
	 *     AAA_bbb_CCC -> aaaBbbCcc
	 *     
	 * example 2) isCamelcase == false
	 *     aaaBbbCcc   -> AAA_BBB_CCC
	 *     AAA_BBB_CCC -> AAA_BBB_CCC
	 *     AAA_bbb_CCC -> AAA_BBB_CCC
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 08. 27.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param text
	 * @return String
	 * </li>
	 * </ul>
	 */
	protected String defaultStringConvert(String text) {
		if(this.isCamelcase) {
			return defaulCamelcase(text);
		}else {
			return defaultUpperUnderscore(text);
		}
	}

	/**
	 * <ul>
	 * <li>업무명 : isAlpaNum</li>
	 * <li>설 명 : isAlpaNum</li>
	 * <li>
	 * <pre>
	 * example)
	 *     queryPageNo   : true
	 *     queryPageNo1  : true
	 *     QUERY_PAGE_NO : false
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 05. 21.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param text
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	private boolean isAlpaNum(String text) {
		Pattern p = Pattern.compile("^[a-zA-Z0-9]*$");
		Matcher m = p.matcher(text);

		return m.matches();
	}

	/**
	 * <ul>
	 * <li>업무명 : isAlpaUpperNum</li>
	 * <li>설 명 : 영문인지 체크 (대문자, 숫자) http://www.unicode.org/charts/PDF/U1100.pdf</li>
	 * <li>
	 * <pre>
	 * example)
	 *     aaaBbbCcc   -> AAA_BBB_CCC
	 *     AAA_BBB_CCC -> AAA_BBB_CCC
	 *     AAA_bbb_CCC -> AAA_BBB_CCC
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 05. 21.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param String
	 * @return boolean
	 * </li>
	 * </ul>
	 */
	private boolean isAlpaUpperNum(String str) {
		boolean isNum = false;
		Pattern p = Pattern.compile("[A-Z0-9]+");
		Matcher m = p.matcher(str);
		if (m.matches()) {
			isNum = true;
		}
		return isNum;
	}

	/**
	 * <ul>
	 * <li>업무명 : convertUnderscoreOrCamel</li>
	 * <li>설 명 : convertUnderscoreOrCamel</li>
	 * <li>
	 * <pre>
	 * example)
	 *     queryPageNo   -> QUERY_PAGE_NO
	 *     QUERY_PAGE_NO -> queryPageNo
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 05. 21.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param text
	 * @return String
	 * </li>
	 * </ul>
	 */
	private String convertUnderscoreOrCamel(String text) {
		String resultText = "";

		if (!isAlpaUpperNum(text) && isAlpaNum(text)) {
			resultText = convertUpperUnderscore(text);
		} else {
			resultText = convertCamelcase(text);
		}

		return resultText;
	}

	/**
	 * <ul>
	 * <li>업무명 : convertCamelcase</li>
	 * <li>설 명 : UPPER_UNDERSCORE to LOWER_CAMEL</li>
	 * <li>
	 * <pre>
	 * example)
	 *     QUERY_PAGE_NO -> queryPageNo
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 05. 21.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param text
	 * @return String
	 * </li>
	 * </ul>
	 */
	private String convertCamelcase(String text) {
		return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, text.toUpperCase());
	}

	/**
	 * <ul>
	 * <li>업무명 : convertUpperUnderscore</li>
	 * <li>설 명 : LOWER_CAMEL to UPPER_UNDERSCORE</li>
	 * <li>
	 * <pre>
	 * example)
	 *     queryPageNo -> QUERY_PAGE_NO
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 05. 21.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @param text
	 * @return String
	 * </li>
	 * </ul>
	 */
	private String convertUpperUnderscore(String text) {
		return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, text);
	}

	/**
	 * <ul>
	 * <li>업무명 : getDataMap(String keysStr)</li>
	 * <li>설 명 : 주어진 key의 value 를 DataMap 새로 설정하여 리턴한다.</li>
	 * <li>작성일 : 2018. 6. 8.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * <pre>
	 * example)
	 * - before
	 * dataMap.put("A", "a");
	 * dataMap.put("B", "b");
	 * dataMap.put("c", "c");
	 * dataMap.put("D", "d");
	 * dataMap.put("E", "e");
	 *
	 * DataMap dataMap = dataMap.getDataMap("A,B,C");
	 *
	 * - after
	 * dataMap.get("A"); -> "a"
	 * dataMap.get("B"); -> "b"
	 * dataMap.get("C"); -> "c"
	 * dataMap.get("D"); -> no key, null
	 * dataMap.get("E"); -> no key, null
	 * </pre>
	 *
	 * </li>
	 * <li>
	 * @param keysStr
	 * @return DataMap
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataMap getDataMap(String keysStr) {
		String[] keys = keysStr.split(",");
		DataMap dataMap = new DataMap();

		for (String key : keys) {
			if (this.get(key) != null) {
				dataMap.put(key, this.get(key));
			}
		}

		return dataMap;
	}
	
	/**
	 * <ul>
	 * <li>업무명 : toDataMapCamelcase</li>
	 * <li>설 명 : DataMap -> DataMap Key UpperUnderscore String</li>
	 * <li>
	 * <pre>
	 * example) key String
	 *     aaaBbbCcc   -> AAA_BBB_CCC
	 *     AAA_BBB_CCC -> AAA_BBB_CCC
	 *     AAA_bbb_CCC -> AAA_BBB_CCC
	 *   
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 08. 27.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @return DataMap
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataMap toDataMapCamelcase() {
		DataMap copy = new DataMap(true);

		Set key = this.keySet();
		String keyName = "";
		String newKeyName = "";

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			keyName = (String) iterator.next();
			newKeyName = defaultStringConvert(keyName);
			copy.put(newKeyName, this.get(keyName));
		}

		return copy;
	}

	/**
	 * <ul>
	 * <li>업무명 : toDataMapUpperUnderscore</li>
	 * <li>설 명 : DataMap -> DataMap Key UpperUnderscore String</li>
	 * <li>
	 * <pre>
	 * example) key String
	 *     aaaBbbCcc   -> AAA_BBB_CCC
	 *     AAA_BBB_CCC -> AAA_BBB_CCC
	 *     AAA_bbb_CCC -> AAA_BBB_CCC
	 *   
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 08. 27.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @return DataMap
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataMap toDataMapUpperUnderscore() {
		DataMap copy = new DataMap(false);

		Set key = this.keySet();
		String keyName = "";
		String newKeyName = "";

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			keyName = (String) iterator.next();
			newKeyName = defaultStringConvert(keyName);
			copy.put(newKeyName, this.get(keyName));
		}

		return copy;
	}

	
	/**
	 * <ul>
	 * <li>업무명 : toHashMap</li>
	 * <li>설 명 : DataMap -> HashMap</li>
	 * <li>
	 * <pre>
	 * example)
	 *   dataMap.toHashMap();
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 05. 21.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @return HashMap
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap toHashMap() {
		HashMap copy = new HashMap();

		Set key = this.keySet();
		String keyName = "";
		String newKeyName = "";

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			keyName = (String) iterator.next();
			newKeyName = defaultStringConvert(keyName);
			copy.put(newKeyName, this.get(keyName));
		}

		return copy;
	}
	
	
	
	/**
	 * <ul>
	 * <li>업무명 : toHashMapCamelcase</li>
	 * <li>설 명 : DataMap -> HashMap Key Camelcase String</li>
	 * <li>
	 * <pre>
	 * example)  key String
	 *     aaaBbbCcc   -> aaaBbbCcc
	 *     AAA_BBB_CCC -> aaaBbbCcc
	 *     AAA_bbb_CCC -> aaaBbbCcc
	 *   
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 08. 27.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @return HashMap
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap toHashMapCamelcase() {
		HashMap copy = new HashMap();

		Set key = this.keySet();
		String keyName = "";
		String newKeyName = "";

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			keyName = (String) iterator.next();
			newKeyName = defaulCamelcase(keyName);
			copy.put(newKeyName, this.get(keyName));
		}

		return copy;
	}
	
	/**
	 * <ul>
	 * <li>업무명 : toHashMapCamelcase</li>
	 * <li>설 명 : DataMap -> HashMap Key UpperUnderscore String</li>
	 * <li>
	 * <pre>
	 * example) key String
	 *     aaaBbbCcc   -> AAA_BBB_CCC
	 *     AAA_BBB_CCC -> AAA_BBB_CCC
	 *     AAA_bbb_CCC -> AAA_BBB_CCC
	 *   
	 * </pre>
	 * </li>
	 * <li>작성일 : 2018. 08. 27.</li>
	 * <li>작성자 : P127602</li>
	 * <li>
	 * @return HashMap
	 * </li>
	 * </ul>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap toHashMapUpperUnderscore() {
		HashMap copy = new HashMap();

		Set key = this.keySet();
		String keyName = "";
		String newKeyName = "";

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			keyName = (String) iterator.next();
			newKeyName = defaultUpperUnderscore(keyName);
			copy.put(newKeyName, this.get(keyName));
		}

		return copy;
	}
}

