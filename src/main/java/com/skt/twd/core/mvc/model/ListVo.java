package com.skt.twd.core.mvc.model;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ListVo<T> extends BaseVo {
	private int size = 0;
	private List<T> content;

	public ListVo () {}
	public ListVo (List<T> content) {
		this.content = content;
		this.size    = content == null ? 0 : content.size();
	}
}
