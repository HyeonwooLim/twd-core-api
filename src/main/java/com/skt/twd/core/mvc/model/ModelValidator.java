package com.skt.twd.core.mvc.model;

import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;

import com.skt.twd.core.annotation.ModelValid;

public class ModelValidator implements ConstraintValidator<ModelValid, Object> {

	@Autowired
	private Validator validator;

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		final Set<ConstraintViolation<Object>> constraintViolations = validator.validate(value);
		if (constraintViolations.size() > 0) {
			constraintViolations.stream().findFirst().ifPresent(cv -> {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(cv.getMessage())
						.addPropertyNode(cv.getPropertyPath().toString()).addConstraintViolation();
			});
			return false;
		}
		return true;
	}

}