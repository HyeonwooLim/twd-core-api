package com.skt.twd.core.mvc.model;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PageVo<T> extends BaseVo {
	private int totalPageNo = 0;
	private int pagePerList = 100;
	private int from = 0;
	private int to = 0;
	private long totalCount = 0;
	private List<T> content;
	
	public PageVo () {}
	
	public PageVo (int from, int to) {
		this.from = from;
		this.to = to;
	}
}
