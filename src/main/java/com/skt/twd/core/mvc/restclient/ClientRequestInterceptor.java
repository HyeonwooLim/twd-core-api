package com.skt.twd.core.mvc.restclient;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class ClientRequestInterceptor implements IRestClientHeaderBinder {

	/** 멤버채널ID */
	public static final String HTTP_HEADER_KEY_MBR_CHANNEL_ID = "TWD-MbrChlId";
	
	/** 입력 채널의 Channel Name 을 전달하기 위한 key */
	public static final String HTTP_HEADER_KEY_CHANNEL_NAME = "TWD-ChannelName";
    
	@Override
	public void addHttpHeader(final HttpServletRequest request, final Map<String, String> header, final byte[] body) {

		/** 맴버채널 아이디 헤더 추가 */
		String keyMbrChlId = HTTP_HEADER_KEY_MBR_CHANNEL_ID;
		String mbrChlId = this.getMbrChlId(request, keyMbrChlId);
		this.addHeaderValueIfExist(header, keyMbrChlId, mbrChlId);

		/** 유입 채널 이름 헤더 추가 */
		String keyChannelName = HTTP_HEADER_KEY_CHANNEL_NAME;
		String channelName = this.getChannelName(request, keyChannelName);
		this.addHeaderValueIfExist(header, keyChannelName, channelName);
	}
	
	/** 맴버채널 아이디 */
	private String getMbrChlId(final HttpServletRequest request, final String key) {
		return request.getHeader(key);
	}

	/**
	 * 유입채널 분석
	 *
	 * @author 
	 * @date 
	 */
	private String getChannelName(final HttpServletRequest orgHttpServletRequest, final String key) {
		return orgHttpServletRequest.getHeader(key);
	}
	
	/** 값이 있을때만 헤더에 추가 */
	private void addHeaderValueIfExist(final Map<String, String> header, final String key, final String value) {
		if (StringUtils.isNotBlank(value)) {
			header.put(key, value);
		}
	}
}
