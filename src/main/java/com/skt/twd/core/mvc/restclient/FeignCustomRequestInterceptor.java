package com.skt.twd.core.mvc.restclient;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alcava00 on 2018. 4. 2..
 */
public class FeignCustomRequestInterceptor implements RequestInterceptor,InitializingBean {
    private static final Logger logger = LoggerFactory.getLogger(FeignCustomRequestInterceptor.class);

    @Autowired
    private ApplicationContext applicationContext;

    private List<IRestClientHeaderBinder> restTemplateHeaderBinderList;

    @Override
    public void apply(RequestTemplate template) {
        HttpServletRequest orgRequest = null;
        if (RequestContextHolder.getRequestAttributes() != null) {
            orgRequest = ((ServletRequestAttributes) RequestContextHolder
                    .getRequestAttributes()).getRequest();
        }
        addHeader(template,orgRequest,template.body());
    }
    private void addHeader(RequestTemplate template ,HttpServletRequest orgRequest, byte[] body) {
        if (restTemplateHeaderBinderList != null) {
            final Map<String, String> headerMap = new HashMap<String, String>();
            restTemplateHeaderBinderList.stream().forEach(restTemplateHeaderBinder -> {
                if (restTemplateHeaderBinder.isSupport(orgRequest,    body)) {
                    restTemplateHeaderBinder.addHttpHeader(orgRequest, headerMap, body);
                }
            });
            headerMap.keySet().stream().forEach(key -> {
                template.header(key, headerMap.get(key));
            });
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        if (restTemplateHeaderBinderList == null) {
            // Find all IExceptionResponseMaker in the ApplicationContext, including ancestor contexts.
            Map<String, IRestClientHeaderBinder> matchingBeans = BeanFactoryUtils
                    .beansOfTypeIncludingAncestors(applicationContext, IRestClientHeaderBinder.class, true, false);
            if (!matchingBeans.isEmpty()) {
                this.restTemplateHeaderBinderList = new ArrayList<IRestClientHeaderBinder>(matchingBeans.values());
                AnnotationAwareOrderComparator.sort(this.restTemplateHeaderBinderList);
            }
        }

        if (restTemplateHeaderBinderList != null) {
            for (IRestClientHeaderBinder er : restTemplateHeaderBinderList) {
                logger.info("add IRestClientHeaderBinder:{}", er.getClass());
            }
        }
    }
}
