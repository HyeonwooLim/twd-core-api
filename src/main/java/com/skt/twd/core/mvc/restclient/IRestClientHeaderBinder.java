package com.skt.twd.core.mvc.restclient;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;

import lombok.extern.slf4j.Slf4j;

public interface IRestClientHeaderBinder extends Ordered {

    void addHttpHeader(final HttpServletRequest orgHttpServletRequest, Map<String, String> header, final byte[] body);

    /**
     * 적용 여부
     * @param orgHttpServletReques
     * @param body
     * @return false return시 미적용
     * */
    default boolean isSupport(final HttpServletRequest orgHttpServletReques, final byte[] body) {
        return true;
    }

    /**
     * 적용 순서
     * @return int
     * */
    default int getOrder() {
        return Integer.MAX_VALUE - 1;
    }

    @Slf4j
    class DefaultRestClientHeaderBinder implements IRestClientHeaderBinder {

        @Override
        public void addHttpHeader(HttpServletRequest orgHttpServletRequest, Map<String, String> returnResponseHeaders, byte[] body) {
            log.debug("start setHttpHeader  >>>> ");
            if (orgHttpServletRequest != null) {
                log.debug("start setHttpHeader  orgHttpServletRequest is not null >>>> ");
                if (orgHttpServletRequest.getHeader(HttpHeaders.COOKIE) != null) {
                	returnResponseHeaders.put(HttpHeaders.COOKIE, orgHttpServletRequest.getHeader(HttpHeaders.COOKIE));
                } else {
                	if(orgHttpServletRequest.getSession() != null) {
                		returnResponseHeaders.put(HttpHeaders.COOKIE, "SESSION=" + orgHttpServletRequest.getSession().getId());
                	}
                }

                if (orgHttpServletRequest.getHeader(HttpHeaders.USER_AGENT) != null) {
                    returnResponseHeaders.put(HttpHeaders.USER_AGENT, orgHttpServletRequest.getHeader(HttpHeaders.USER_AGENT));
                }
            }
        }
    }
}