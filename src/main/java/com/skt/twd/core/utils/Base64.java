package com.skt.twd.core.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Base64
 *
 * @author SOC0865
 *
 */
public class Base64 {

	/**
	 * 인코딩
	 *
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String encodeBase64(String str) throws Exception {
		String result = "";

		if (str != null) {

			byte[] buf = null;

			BASE64Encoder base64Encoder = new BASE64Encoder();
			ByteArrayInputStream bin = new ByteArrayInputStream(str.getBytes());
			ByteArrayOutputStream bout = new ByteArrayOutputStream();

			try {
				base64Encoder.encodeBuffer(bin, bout);

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			buf = bout.toByteArray();
			result = new String(buf).trim();

		}

		return result;
	}

	/**
	 * 디코딩
	 *
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String decodeBase64(String str) throws Exception {
		String result = "";

		if (str != null) {
			byte[] buf = null;

			BASE64Decoder base64Decoder = new BASE64Decoder();

			try {
				buf = base64Decoder.decodeBuffer(str);
			} catch (Exception e) {
				e.printStackTrace();
			}
			result = new String(buf, "UTF-8").trim();
			// result = new String(buf).trim();
		}
		return result;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "84010111695111";

		try {
			String enc = Base64.encodeBase64(str);

			System.out.println("enc : " + enc);
			System.out.println("dec : " + Base64.decodeBase64(enc));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}