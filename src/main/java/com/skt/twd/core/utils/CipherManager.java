package com.skt.twd.core.utils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cubeone.CubeOneAPI;
import com.skt.twd.core.AppConfig;

import cryptix.provider.key.RawSecretKey;
import cryptix.util.core.Hex;
import xjava.security.Cipher;

public class CipherManager {

	private static Logger log = LoggerFactory.getLogger(CipherManager.class);

	public static CipherManager instance;
	private static AppConfig appConfig;

	private static final Charset DEFAULT_CHARSET = Charset.forName("EUC-KR");

	/**
	 * Tworld 암호화 Key
	 */
	private final static String MKEY_TPOINT = "203030303032333230305892"; // T포인트 제휴 암호화 키 (20151001)

	public static void setAppConfig(AppConfig appConf) {
		appConfig = appConf;
	}

	public static CipherManager getInstance() {
		if (instance == null)
			instance = new CipherManager();
		return instance;
	}

	/**
	 *
	 * <pre>
	 * HEX Type 데이터를 암호화 된 데이터로 변환
	 * </pre>
	 *
	 * @param input
	 * @param in
	 * @return
	 */
	public String encrypt(String input, String in) {
		byte[] ect = null;
		String message = "";

		try {
			message = new String(input.getBytes(DEFAULT_CHARSET), StandardCharsets.ISO_8859_1);
			/***** 메세지 길이 체크(8의 배수여야 한다) *****/
			while ((message.length() % 8) != 0) message += " ";

			Cipher alg = Cipher.getInstance("DES-EDE3", "Cryptix");
			RawSecretKey key = new RawSecretKey("DES_EDE3", in.getBytes());

			alg.initEncrypt(key);
			ect = alg.crypt(message.getBytes(StandardCharsets.ISO_8859_1));
			if (log.isDebugEnabled()) {
				log.debug("## DECRYPT : DES-EDE3 - Cryptix : " + ect);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return Hex.toString(ect);
	}

	/**
	 *
	 * <pre>
	 * 암호화 된 데이터를 원래의 HEX Type으로 복호화
	 * </pre>
	 *
	 * @param input
	 * @param in
	 * @return
	 */
	public String decrypt(String input, String in) {
		return decrypt(input, in, DEFAULT_CHARSET);
	}

	/**
	 *
	 * <pre>
	 * 암호화 된 데이터를 원래의 HEX Type으로 복호화
	 * </pre>
	 *
	 * @param input
	 * @param in
	 * @return
	 */
	public String decrypt(String input, String in, Charset charset) {
		byte[] dct = null;

		try {
			Cipher alg = Cipher.getInstance("DES-EDE3", "Cryptix");
			RawSecretKey key = new RawSecretKey("DES_EDE3", in.getBytes());

			alg.initDecrypt(key);
			dct = alg.crypt(Hex.fromString(input));

			if (log.isDebugEnabled()) {
				log.debug("## DECRYPT : DES-EDE3 - Cryptix : " + dct);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return new String(dct, charset).trim();
	}

	/**
	 * CubeOneAPI encrypt
	 *
	 * @param input
	 * @return
	 */
	public String encrypt(String input) {
		String ect = "";

		try {
			input = (input == null) ? "" : input.trim(); // StringUtil.strNull(input);
//        	if(true){return input;}
			if ("".equals(input)) {
				return input;
			}

			if (appConfig.isLocal()) {
				ect = input;
			} else {
				ect = CubeOneAPI.jcoencrypt(input);
			}

			// System.out.println(input + " : " + ect);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ect.trim();
	}

	/**
	 * CubeOneAPI decrypt
	 *
	 * @param input
	 * @return
	 */
	public String decrypt(String input) {
		String dct = "";
		try {
			input = (input == null) ? "" : input.trim(); // StringUtil.strNull(input);
//        	if(true){return input;}
			if ("".equals(input)) {
				return input;
			}

			if (appConfig.isLocal()) {
				dct = input;
			} else {
				dct = CubeOneAPI.jcodecrypt(input);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dct;
	}

	public static String getMkeyTpoint() {
		return MKEY_TPOINT;
	}
}
