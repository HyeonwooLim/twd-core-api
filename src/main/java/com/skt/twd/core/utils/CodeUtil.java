package com.skt.twd.core.utils;

import java.util.List;
import java.util.Optional;

import com.skt.twd.cache.CacheConstants.REDIS_CACHE;
import com.skt.twd.cache.common.utils.CacheUtils;
import com.skt.twd.cache.model.Code;
import com.skt.twd.cache.model.CodeCategory;

import lombok.extern.slf4j.Slf4j;

/**
 * Redis Cache Code 조회
 */
@Slf4j
public class CodeUtil {
	/**
	 * CodeCategory 조회
	 * @param lCd
	 * @return CodeCategory
	 */
	public static CodeCategory get(String lCd) {
		return CacheUtils.getCacheData(REDIS_CACHE.Code, lCd, CodeCategory.class);
	}

	/**
	 * Code List 조회
	 * @param lCd
	 * @return List
	 */
	public static List<Code> getCodes(String lCd) {
		CodeCategory codeCategory = CodeUtil.get(lCd);
		List<Code> codes = codeCategory.getCodes();

		return codes;
	}

	/**
	 * Code 조회
	 * @param lCd
	 * @param mCd
	 * @return Code
	 */
	public static Code getCode(String lCd, String mCd) {
		CodeCategory codeCategory = CodeUtil.get(lCd);
		Optional<Code> op =  codeCategory.getCodes().stream().filter(c -> c.getMCd().equals(mCd)).findAny();

		Code code = null;
		if(op.isPresent()) {
			code = op.get();
		}

		return code;
	}
}
