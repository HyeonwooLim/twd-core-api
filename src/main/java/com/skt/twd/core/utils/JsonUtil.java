package com.skt.twd.core.utils;

import java.io.IOException;

import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonUtil {

	private static ObjectMapper objectMapper;

	public static void setObjectMapper(ObjectMapper objectMapper) {
		if (JsonUtil.objectMapper == null) {
			if (objectMapper == null) {
				JsonUtil.objectMapper = Jackson2ObjectMapperBuilder.json().createXmlMapper(false).build();
				log.debug("Jackson2ObjectMapperBuilder.json().createXmlMapper(false) : {}", JsonUtil.objectMapper);
			} else {
				JsonUtil.objectMapper = objectMapper;
				log.debug("objectMapper : {}", JsonUtil.objectMapper);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T convertValue(Object object, Class<?> clazz){
		try {
			return (T) JsonUtil.objectMapper.convertValue(object, clazz);
		} catch (Exception e) {
			log.error("Exception", e);
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T> T toObject(String json, Class<?> clazz) {
		try {
			return (T) JsonUtil.objectMapper.readValue(json, clazz);
		} catch (JsonParseException e) {
			log.error("JsonParseException", e);
		} catch (JsonMappingException e) {
			log.error("JsonMappingException", e);
		} catch (IOException e) {
			log.error("IOException", e);
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T> T toObject(String json,  TypeReference<T> clazz) {
		try {
			return (T) JsonUtil.objectMapper.readValue(json, clazz);
		} catch (JsonParseException e) {
			log.error("JsonParseException", e);
		} catch (JsonMappingException e) {
			log.error("JsonMappingException", e);
		} catch (IOException e) {
			log.error("IOException", e);
		}

		return null;
	}

	public static String toJsonString(Object object) {
		try {
			return JsonUtil.objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException", e);
		}

		return "";
	}
}
