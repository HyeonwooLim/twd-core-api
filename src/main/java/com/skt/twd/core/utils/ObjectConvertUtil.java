package com.skt.twd.core.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeanUtils;

import com.google.common.base.CaseFormat;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings("unchecked")
public class ObjectConvertUtil {
	public static final String ACCESS_TYPE_FIELD = "FIELD"; // ACCESS_TYPE_FIELD : public 변수만 가능하다.
	public static final String ACCESS_TYPE_METHOD = "METHOD";

	public static <T> T newInstance(Class<?> clazz) {
		Object targetObj = null;
		String className = clazz.getName();

		try {
			Class<?> c = Class.forName(className);
			targetObj = c.newInstance();
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (InstantiationException e) {
			log.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
		}

		return (T) targetObj;
	}

	public static <T> T toTargetObject(Object srcObj, Class<?> clazz) {
		Object targetObj = ObjectConvertUtil.newInstance(clazz);
		BeanUtils.copyProperties(srcObj, targetObj, clazz);
		return (T) targetObj;
	}

	public static <T> T toTargetObjectDefaultValue(Object srcObj, Class<?> clazz) {
		Field[] fields 	   = srcObj.getClass().getDeclaredFields();
		String fieldName   = null;
		String fnFieldName = null;
		Method getMethod   = null;
		Method setMethod   = null;
		Object value       = null;

		try {
			for (Field f : fields) {
				String fieldType = f.getType().getSimpleName();
				fieldName = f.getName();
				fnFieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
				getMethod = BeanUtils.findMethod(srcObj.getClass(), "get" + fnFieldName);
				setMethod = BeanUtils.findMethodWithMinimalParameters(srcObj.getClass(), "set" + fnFieldName);
				value = getMethod.invoke(srcObj);
				if (value == null) {
					if (String.class.getSimpleName().equals(fieldType)) {
						value = "";
					} else if (Integer.class.getSimpleName().equals(fieldType)
							|| Long.class.getSimpleName().equals(fieldType)) {
						value = 0;
					}
				}
				log.debug("==>> toTargetObjectDefaultValue Field : {}, {}, {}", f.getType(), f.getName(), value);
				setMethod.invoke(srcObj, value);
			}
		} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
			log.error(e.getMessage(), e);
		}

		return ObjectConvertUtil.toTargetObject(srcObj, clazz);
	}

	public static Map<String, Object> convertObjectToMap(Object obj, String accessType) {
		return convertObjectToHashMap(obj, null, accessType);
	}

	public static Map<String, Object> convertObjectToMapUpperUnderscore(Object obj, String accessType) {
		return convertObjectToHashMap(obj, CaseFormat.UPPER_UNDERSCORE, accessType);
	}

	public static Map<String, Object> convertObjectToMapLowerUnderscore(Object obj, String accessType) {
		return convertObjectToHashMap(obj, CaseFormat.LOWER_UNDERSCORE, accessType);
	}

	public static HashMap<String, Object> convertObjectToHashMap(Object obj, String accessType) {
		return convertObjectToHashMap(obj, null, accessType);
	}

	public static HashMap<String, Object> convertObjectToHashMapUpperUnderscore(Object obj, String accessType) {
		return convertObjectToHashMap(obj, CaseFormat.UPPER_UNDERSCORE, accessType);
	}

	public static HashMap<String, Object> convertObjectToHashMapLowerUnderscore(Object obj, String accessType) {
		return convertObjectToHashMap(obj, CaseFormat.LOWER_UNDERSCORE, accessType);
	}

	public static HashMap<String, Object> convertObjectToHashMap(Object obj, CaseFormat caseFormat, String accessType) {
		if (ACCESS_TYPE_FIELD.equals(accessType)) {
			return convertObjectFieldToHashMap(obj, caseFormat);
		} else {
			return convertObjectMethodToHashMap(obj, caseFormat);
		}
	}

	public static HashMap<String, Object> convertObjectFieldToHashMap(Object obj, CaseFormat caseFormat) {
		log.debug("{}", obj);

		HashMap<String, Object> map = new HashMap<String, Object>();
		Field[] fields = obj.getClass().getDeclaredFields();
		String fieldName = null;

		try {
			for (Field f : fields) {
				log.debug("==>> convertObjectFieldToHashMap Field : {}, {}", f.getName(), f.get(obj));
				fieldName = f.getName();
				if (caseFormat != null) {
					if (caseFormat.equals(CaseFormat.UPPER_UNDERSCORE)) {
						fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, fieldName);
					} else if (caseFormat.equals(CaseFormat.LOWER_UNDERSCORE)) {
						fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldName);
					}
				}
				map.put(fieldName, f.get(obj));
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			log.error(e.getMessage(), e);
		}
		log.debug("==> convertObjectFieldToHashMap {}", map);
		return map;
	}

	public static HashMap<String, Object> convertObjectMethodToHashMap(Object obj, CaseFormat caseFormat) {
		log.debug("{}", obj);

		HashMap<String, Object> map = new HashMap<String, Object>();
		Method[] methods = obj.getClass().getDeclaredMethods();
		String methodName = null;
		String fieldName = null;

		try {
			for (Method m : methods) {
				methodName = m.getName();
				if (methodName.startsWith("get")) {
					fieldName = methodName.substring(3, 4).toLowerCase() + methodName.substring(4, methodName.length());
					if (caseFormat != null) {
						if (caseFormat.equals(CaseFormat.UPPER_UNDERSCORE)) {
							fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, fieldName);
						} else if (caseFormat.equals(CaseFormat.LOWER_UNDERSCORE)) {
							fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldName);
						}
					}
					map.put(fieldName, m.invoke(obj));
				}
			}
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			log.error(e.getMessage(), e);
		}

		log.debug("==> convertObjectMethodToHashMap {}", map);

		return map;
	}
}
