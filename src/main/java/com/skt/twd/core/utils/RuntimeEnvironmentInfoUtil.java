package com.skt.twd.core.utils;

import java.net.InetAddress;
import java.util.Optional;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.ClassUtils;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by alcava00 on 2018. 4. 16..
 */
public class RuntimeEnvironmentInfoUtil {

	private final static String _NODE_HOSTNAME = "NODE_HOSTNAME";  // -->
	private final static String _POD_NAMESPACE = "POD_NAMESPACE"; //
	private final static String _POD_NAME = "POD_NAME"; //-->
	private final static String _POD_IP = "POD_IP"; //
	private final static String _CONTAINER_NAME = "CONTAINER_NAME"; //
	private final static String hostName;
	private static final String nodeHostname;
	private static final String podNameSpace;
	private static final String podName;
	private static final String podIp;
	private static final String containerName;
	private static String basePackageName;

	static {
		nodeHostname = Optional.ofNullable(System.getenv(_NODE_HOSTNAME)).orElse("nodeHostname");
		podNameSpace = Optional.ofNullable(System.getenv(_POD_NAMESPACE)).orElse("podNameSpace");
		podName = Optional.ofNullable(System.getenv(_POD_NAME)).orElseGet(() -> getLocalHostName());
		podIp = Optional.ofNullable(System.getenv(_POD_IP)).orElse("podIp");
		containerName = Optional.ofNullable(System.getenv(_CONTAINER_NAME)).orElse("containerName");
		hostName = getLocalHostName();
	}

	public static String getBasePackageName() {
		if (basePackageName == null) {
			WebApplicationContext applicationContext = ContextLoader.getCurrentWebApplicationContext();
			String[] name = applicationContext.getBeanNamesForAnnotation(SpringBootApplication.class);
			if (name != null && name.length > 0) {
				basePackageName = ClassUtils.getPackageName(applicationContext.getBean(name[0]).getClass());
			}
		}

		return basePackageName;
	}

	private static String getMainClassName() {
		StackTraceElement trace[] = Thread.currentThread().getStackTrace();
		if (trace.length > 0) {
			return trace[trace.length - 1].getClassName();
		}
		return "Unknown";
	}

	public static String getNodeHostname() {
		return nodeHostname;
	}

	public static String getPodNameSpace() {
		return podNameSpace;
	}

	public static String getPodName() {
		return podName;
	}

	public static String getPodIp() {
		return podIp;
	}

	public static String getContainerName() {
		return containerName;
	}

	public static String getHostName() {
		return hostName;
	}

	private static String getLocalHostName() {
		try {
			return (InetAddress.getLocalHost()).getHostName();
		} catch (Exception uhe) {
			String host = uhe.getMessage(); // host = "hostname: hostname"
			if (host != null) {
				int colon = host.indexOf(':');
				if (colon > 0) {
					return host.substring(0, colon);
				}
			}
			return "unknownhost";

		}
	}
}
