package com.skt.twd.core.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 라이트할부/쓰던폰 반납 암호화 유틸
 *
 * @description : TWD TwdAES128Util
 * @author P083490
 * @since 2017. 12. 28.
 * @version 1.0
 * @see Copyright (C) by SKT corp. All right reserved.
 * @modification information
 *
 * 수정일         수정자         수정내용
 * --------- ------------ -------------------------------
 * 2017. 12. 28.                  최초작성
 * 정책팀-강인혁M / 이종길 / CHG610000055390 / "TWD라이트할부, 쓰던폰반납" / 20171228
 *
 */
public class TwdAES128Util {
	private static Logger log = LoggerFactory.getLogger(TwdAES128Util.class);

	private static String transformationRule = "AES/CBC/PKCS5Padding";
	private static String IV = "ABCDEFGHIJKLMNOP";

	private static String TWD_KEY = "7ltg8Zmdge&)LYVd"; // 암호화 key

	/**
	 * 암호화
	 *
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public String encode(String str) throws Exception {
		return encryptAES(str, TWD_KEY);
	}

	/**
	 * 복호화
	 *
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public String decode(String str) throws Exception {
		return decryptAES(str, TWD_KEY);
	}

	/**
	 * @param args
	 */
	// key는 16바이트로 구성 되어야 한다.
	public String encryptAES(String s, String key) throws Exception {

		String encrypted = null;

		try {

			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			IvParameterSpec ivParameterSpec = new IvParameterSpec(IV.getBytes("UTF-8"));

			Cipher cipher = Cipher.getInstance(transformationRule);
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
			encrypted = byteArrayToHex(cipher.doFinal(s.getBytes("UTF-8")));

			return encrypted;

		} catch (Exception e) {
			throw e;
		}
	}

	// key는 16 바이트로 구성 되어야 한다.
	public String decryptAES(String s) throws Exception {
		String key = TWD_KEY;
		String decrypted = null;

		try {

			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance(transformationRule);
			IvParameterSpec ivParameterSpec = new IvParameterSpec(IV.getBytes("UTF-8"));

			cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
			decrypted = new String(cipher.doFinal(hexToByteArray(s)), "UTF-8");

			return decrypted;

		} catch (Exception e) {
			throw e;
		}
	}

	// key는 16 바이트로 구성 되어야 한다.
	public String decryptAES(String s, String key) throws Exception {

		String decrypted = null;

		try {

			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance(transformationRule);
			IvParameterSpec ivParameterSpec = new IvParameterSpec(IV.getBytes("UTF-8"));

			cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
			decrypted = new String(cipher.doFinal(hexToByteArray(s)), "UTF-8");

			return decrypted;

		} catch (Exception e) {
			throw e;
		}
	}

	private byte[] hexToByteArray(String s) {

		byte[] retValue = null;

		if (s != null && s.length() != 0) {
			retValue = new byte[s.length() / 2];

			for (int i = 0; i < retValue.length; i++) {
				retValue[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2), 16);
			}

		}
		return retValue;
	}

	private String byteArrayToHex(byte buf[]) {

		StringBuffer strbuf = new StringBuffer(buf.length * 2);

		for (int i = 0; i < buf.length; i++) {

			if (((int) buf[i] & 0xff) < 0x10) {
				strbuf.append("0");
			}

			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}

		return strbuf.toString();
	}

	public static void main(String[] args) {
		TwdAES128Util util = new TwdAES128Util();
		try {
			System.out.println(util.decryptAES(
					"7b67e783c7d945bbacc5829fb858ae7d37ddd8d5ca24d4c49e0a5e623118dd2de4d1dd61a813f0525e564bccfcb78bebce72cd21e1f84ad604b62b493ba79c7793edc0f5e4c2b04db909eccde8b8606db467a85abb439a8a23f7ffe2b0022422b4bc108df24d699355d30a3a3a66601e284c596272cb31e5e07e18a2d141bb844637dfdf4fc579300f6be640e1c7380259df8289de1631ab905ab431a251c81b9598d81d6e98bd1f5fc931953b6522352154eb26a00b600edc70ffd882429ef516b7e246fb516efed49f9132e4fc5d6794eb254916f16459a3b512379a274d68f192075fc034ecfe8ffd38f92931d22d12da195e591f7ce4835a366835765943da8bef42022ee592cedf4bfaf7d58663b6ac709e2612fd18629034516cbc12a9af8d5c0e9baad9bec6c6254ee47d153847a3479ef4eeffb1bb0f0430b052ace9418641b3eb02a732c43a16b9c31cdc0e64db41811db50f8bcc3e4d80660beaba242d6b815593010836c180f26c195d886681018bba82ff814411a027c0c54a92908d415be0b591e30a531c29a43752c8a07b81a63ec2befd13c8d6d72811db6a",
					"7ltg8Zmdge&)LYVd"));
		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}

}