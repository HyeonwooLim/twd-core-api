package com.skt.twd.core.utils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

import com.skt.twd.core.exceptions.BizException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ValidatorUtil {

	@Autowired
	private static ApplicationContext ctx;

	public static void setApplicationContext(ApplicationContext ctx) {
		ValidatorUtil.ctx = ctx;
	}

	public static void valid(Object object, Class<? extends Validator> clazz) {
		Validator validator = (Validator) ctx.getBean(clazz);
		ValidatorUtil.valid(object, validator);
	}

	public static void valid(Object object, Validator validator) {
		BindingResult bindingResult = new DataBinder(object).getBindingResult();
		validator.validate(object, bindingResult);

		if (bindingResult.hasErrors()) {
			ObjectError objectError = bindingResult.getAllErrors().stream().findFirst().get();

			if (objectError.getArguments() != null) {
				throw new BizException(objectError.getCode(), objectError.getArguments());
			} else if(StringUtils.isNotBlank(objectError.getDefaultMessage())){
				throw new BizException(objectError.getCode(), objectError.getDefaultMessage());
			} else {
				throw new BizException(objectError.getCode());
			}
		}
	}

	public static void validBean(Object object) {
		validatedBean(object);
	}

	public static void validatedBean(Object object, Class<?>... groups) {
		javax.validation.Validation
						.buildDefaultValidatorFactory()
						.getValidator()
						.validate(object, groups)
						.stream()
						.findFirst()
						.ifPresent(cv -> {
							log.debug("{} : {} - {}, {}", cv.getRootBean().getClass().getName(), cv.getPropertyPath(), cv.getMessage(), cv.getRootBean());
							throw new BizException(cv.getMessage());
						});
	}
}
