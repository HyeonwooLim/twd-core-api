package com.skt.twd.cache;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;

import com.skt.twd.TwdApiCoreApplicationTests;
import com.skt.twd.cache.CacheConstants;
import com.skt.twd.cache.CacheConstants.REDIS_CACHE;
import com.skt.twd.cache.common.utils.CacheUtils;
import com.skt.twd.cache.model.ApiMeta;
import com.skt.twd.cache.model.Code;
import com.skt.twd.cache.model.CodeCategory;
import com.skt.twd.cache.model.TssMeta;
import com.skt.twd.cache.vo.CmsComponentEntityVo;
import com.skt.twd.core.mvc.BlockApiComponent;
import com.skt.twd.core.utils.CodeUtil;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TwdApiCoreApplicationTests.class)
@Slf4j
public class CacheUtilsTests {

	@Autowired
	BlockApiComponent blockApiComponent;

	@Test
	public void codeTests() {
		String lCd = "SM272";
		String mCd = "02";
		CodeCategory codeCategory = null;
		List<Code> codes = null;
		Code code = null;

		log.info("--->>> start code category <<<---");
		codeCategory = CodeUtil.get(lCd);
		log.info("--->>>codeCategory 1: {}", codeCategory);

		codeCategory = CacheUtils.getCacheData(REDIS_CACHE.Code, lCd, CodeCategory.class);
		log.info("--->>>codeCategory 2: {}", codeCategory);

		log.info("--->>> start codes <<<---");
		codes = CodeUtil.getCodes(lCd);
		log.info("--->>>codes 1: {}", codes);

		codeCategory = CacheUtils.getCacheData(REDIS_CACHE.Code, lCd, CodeCategory.class);
		codes = codeCategory.getCodes();
		log.info("--->>>codes 2: {}", codes);

		log.info("--->>> start code <<<---");
		code = CodeUtil.getCode(lCd, mCd);
		log.info("--->>>code 1: {}", code);

		codeCategory = CacheUtils.getCacheData(REDIS_CACHE.Code, lCd, CodeCategory.class);
		codes = codeCategory.getCodes();
		Optional<Code> op =  codeCategory.getCodes().stream().filter(c -> c.getMCd().equals(mCd)).findAny();
		if(op.isPresent()) {
			code = op.get();
			log.info("--->>>code 2: {}", code);
		}
	}

	@Test
	public void contextLoads() {
		log.info("--->>> REDIS_CACHE.ApiMeta : {}", REDIS_CACHE.ApiMeta);
		log.info("--->>> REDIS_CACHE.ApiMeta.name() : {}", REDIS_CACHE.ApiMeta.name());

		CmsComponentEntityVo componentEntityVo = CacheUtils.getCacheData(CacheConstants.REDIS_CACHE.CmsComponent, "CMW00012/1", CmsComponentEntityVo.class);
		log.info("--->>> componentEntityVo : {}", componentEntityVo);


		ApiMeta apiMeta = CacheUtils.getCacheData(REDIS_CACHE.ApiMeta, "GET|/customer", ApiMeta.class);
		log.info("--->>> apiMeta : {}", apiMeta);

		TssMeta tssMeta = CacheUtils.getCacheData(CacheConstants.REDIS_CACHE.TssMeta, CacheConstants.REDIS_CACHE.TssMeta.name().toLowerCase(), TssMeta.class);
		log.info("--->>> tssMeta : {}", tssMeta);

		blockApiComponent.check(HttpMethod.GET, "/customer");
	}
}
