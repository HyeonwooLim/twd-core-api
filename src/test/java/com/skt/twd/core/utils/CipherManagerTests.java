package com.skt.twd.core.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.skt.twd.core.utils.CipherManager;
import com.skt.twd.core.utils.old.CpCipherManager;
import com.skt.twd.core.utils.old.OldCipherManager;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@Slf4j
public class CipherManagerTests {
	@Test
	public void case1Tests() {
		String paramStr = "AAA";
		log.info("--->>> paramStr : {}", paramStr);
		String encryptStr = CipherManager.getInstance().encrypt(paramStr,  CipherManager.getMkeyTpoint());
		log.info("--->>> encryptStr : {}", encryptStr);
		String decryptStr = CipherManager.getInstance().decrypt(encryptStr,  CipherManager.getMkeyTpoint());
		log.info("--->>> decryptStr : {}", decryptStr);
	}

	@Test
	public void confirmTests() {
		String result = "실패!!";
		String orgTradeId = "100002091241";
		String encryptParam = "trade_id=10000000000002301616&org_trade_id="+orgTradeId;

		String newEncryptStr = CipherManager.getInstance().encrypt(encryptParam,  CipherManager.getMkeyTpoint());		// 최종적용
		String oldEncryptStr = OldCipherManager.getInstance().encrypt(encryptParam,  OldCipherManager.getMkeyTpoint()); // 원본
		String cpEncryptStr  = CpCipherManager.getInstance().encrypt(encryptParam,  CpCipherManager.getMkeyTpoint());	// 상품에서 가져온거

		String newDecryptStr = CipherManager.getInstance().decrypt(newEncryptStr,  CipherManager.getMkeyTpoint());		// 최종적용
		String oldDecryptStr = OldCipherManager.getInstance().decrypt(oldEncryptStr,  OldCipherManager.getMkeyTpoint()); // 원본
		String cpDecryptStr  = CpCipherManager.getInstance().decrypt(cpEncryptStr,  CpCipherManager.getMkeyTpoint());

		if(newEncryptStr.equals(oldEncryptStr)
				&& oldEncryptStr.equals(cpEncryptStr)
				&& newEncryptStr.equals(cpEncryptStr)) {
			result = "성공!!!";
		}

		log.info(result);
		log.info(newEncryptStr);
		log.info(oldEncryptStr);
		log.info(cpEncryptStr);

		assertEquals(newEncryptStr, oldEncryptStr);
		assertEquals(oldEncryptStr, cpEncryptStr);
		assertEquals(newEncryptStr, cpEncryptStr);

		result = "실패!!";
		if(newDecryptStr.equals(oldDecryptStr)
				&& oldDecryptStr.equals(cpDecryptStr)
				&& newDecryptStr.equals(cpDecryptStr)) {
			result = "성공!!!";
		}

		log.info(result);
		log.info(newDecryptStr);
		log.info(oldDecryptStr);
		log.info(cpDecryptStr);

		assertEquals(newDecryptStr, oldDecryptStr);
		assertEquals(oldDecryptStr, cpDecryptStr);
		assertEquals(newDecryptStr, cpDecryptStr);
	}
}
