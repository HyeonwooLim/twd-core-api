package com.skt.twd.core.utils;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.skt.twd.core.mvc.model.DataMap;
import com.skt.twd.core.utils.DataMap2Tests.DataMap2.StringMode;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@Slf4j
public class DataMap2Tests {

	@Test
	public void case1() {
		HashMap<String, String> hmap = new HashMap<>();
		hmap.put("key1Name", "value1Name");
		hmap.put("key2Name", "value2Name");
		hmap.put("key3_Name", "value3_Name");

		DataMap2<String, String> dmap = new DataMap2<>(hmap, StringMode.Ignore);

		log.debug("--->>> hmap : {}", hmap);
		log.debug("--->>> dmap : {}", dmap);
		assertEquals(hmap.get("key1"), dmap.get("key1"));
		assertEquals(hmap.get("key2"), dmap.get("key2"));
		assertEquals(hmap.get("key3"), dmap.get("key3"));
	}

	public static class DataMap2<K, V> extends DataMap<K, V> {

		public enum StringMode {
			Camelcase,
			UpperUnderscore,
			Ignore
		}

		private StringMode stringMode;

		public DataMap2(HashMap map, StringMode stringMode) {
			this.stringMode = stringMode;
			Set key = map.keySet();
			String keyName = "";
			String newKeyName = "";

			for (Iterator iterator = key.iterator(); iterator.hasNext();) {
				keyName = (String) iterator.next();
				newKeyName = defaultStringConvert(keyName);
				this.put((K) newKeyName, (V) map.get(keyName));
			}
		}

		@Override
		protected String defaultStringConvert(String text) {
			if(this.stringMode == StringMode.Camelcase) {
				return defaulCamelcase(text);
			}else if(this.stringMode == StringMode.UpperUnderscore) {
				return defaultUpperUnderscore(text);
			}else {
				return text;
			}
		}
	}
}
