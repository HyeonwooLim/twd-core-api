package com.skt.twd.core.utils;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.skt.twd.core.ConfigProfiles;
import com.skt.twd.core.mvc.model.DataMap;
import com.skt.twd.core.utils.JsonUtil;
import com.skt.twd.core.utils.vo.TestVo;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@Slf4j
@ActiveProfiles(ConfigProfiles.LOCAL)
public class JsonUtilTests {

	@Before
	public void setUp() {
		JsonUtil.setObjectMapper(null);
	}

	@Test
	public void jsonutil_StringToMapTest() {
		String json = "{\"queryPageNo\":\"1\", \"queryStartTime\":\"20070405000000\", \"queryEndTime\":\"20070405235959\"}";
		HashMap<String, Object> map = JsonUtil.toObject(json, HashMap.class);
		log.debug("jsonutil_StringToMapTest : {}", map);
	}

	@Test
	public void jsonutil_StringToDataMapTest() {
		String json = "{\"queryPageNo\":\"1\", \"queryStartTime\":\"20070405000000\", \"queryEndTime\":\"20070405235959\"}";
		DataMap map = JsonUtil.toObject(json, DataMap.class);
		log.debug("jsonutil_StringToDataMapTest : {}", map);
	}

	@Test
	public void jsonutil_StringToVoTest() {
		String json = "{\"queryPageNo\":\"1\", \"queryStartTime\":\"20070405000000\"}";
		TestVo paramVo = JsonUtil.toObject(json, TestVo.class);
		log.debug("jsonutil_StringToVoTest : {}", paramVo);
	}

	@Test
	public void jsonutil_VoToStringTest() {
		TestVo paramVo = new TestVo();
		paramVo.setQueryPageNo(1);
		paramVo.setQueryStartTime("20070405000000");
		paramVo.setQueryEndTime("20070405235959");

		String json = JsonUtil.toJsonString(paramVo);

		log.debug("jsonutil_VoToStringTest : {}", json);
	}

	@Test
	public void jsonutil_MapToStringTest() {
		HashMap paramMap = new HashMap();
		paramMap.put("queryPageNo", 1);
		paramMap.put("queryStartTime", "20070405000000");
		paramMap.put("queryEndTime", "20070405235959");

		String json = JsonUtil.toJsonString(paramMap);

		log.debug("jsonutil_MapToStringTest : {}", json);
	}
}
