package com.skt.twd.core.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.skt.twd.core.mvc.ObjectWrapper;
import com.skt.twd.core.utils.ObjectConvertUtil;
import com.skt.twd.order.vo.ATest;
import com.skt.twd.order.vo.BTest;
import com.skt.twd.order.vo.CTest;
import com.skt.twd.order.vo.ItemTestVo;
import com.skt.twd.order.vo.OrderInVoTest;
import com.skt.twd.order.vo.OrderItemVoTest;
import com.skt.twd.order.vo.OrderVoTest;
import com.skt.twd.order.vo.PubTestVo;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@Slf4j
//@Ignore
public class ObejctConvertUtilTests {

	OrderInVoTest orderInVoTest;
	ObjectWrapper orderVoWrapper;

	@Before
    public void setUp() throws Exception {
		orderInVoTest = new OrderInVoTest();

		OrderVoTest orderVoTest = new OrderVoTest();
		orderVoTest.setAddress("address");
		orderVoTest.setUserId("1234");
		orderVoTest.setRegDate(new Date());
		orderInVoTest.setOrderVoTest(orderVoTest);

		OrderItemVoTest orderItemVoTest = new OrderItemVoTest();
		orderItemVoTest.setAddServiceId("addServiceId");
		orderItemVoTest.setProductId("productId");
		orderInVoTest.setOrderItemVoTest(orderItemVoTest);

		orderVoWrapper  = new ObjectWrapper(orderInVoTest);
    }

	@Test
	public void convertObjectListTest() {
		ItemTestVo item = new ItemTestVo();
		item.setName("Test Name...");

		List<ItemTestVo> list = new ArrayList<>();
		list.add(item);

		ATest at = new ATest();
		at.setMemo("A Memo...");
		at.setList(list);

		ObjectWrapper testWrapper  = new ObjectWrapper(at);
		BTest bt = testWrapper.convertObject(BTest.class);

		ObjectWrapper testWrapper2  = new ObjectWrapper(at);
		CTest ct = testWrapper2.convertObject(CTest.class);

		log.debug("--->>> Case A -> B : {}", bt);
		assertEquals(at.getMemo(), bt.getMemo());

		log.debug("--->>> Case A -> C : {}", ct);
		assertEquals(at.getMemo(), ct.getMemo());
		assertSame(at.getList(), ct.getList());
		assertSame(at.getList().get(0).getName(), ct.getList().get(0).getName());
	}

	/**
	 * orderVoWrapper.convertObject
	 */
	@Test
	public void convertObjectTest() {
		OrderInVoTest targetOrderVoTest = orderVoWrapper.convertObject(OrderInVoTest.class);
		log.debug("targetOrderVoTest {}", targetOrderVoTest.toString());

		log.debug("targetOrderVoTest.getOrderVoTest {}", targetOrderVoTest.getOrderItemVoTest().toString());

		assertEquals(orderInVoTest.getOrderVoTest().getAddress(), targetOrderVoTest.getOrderVoTest().getAddress());
		assertEquals(orderInVoTest.getOrderVoTest().getUserId(),  targetOrderVoTest.getOrderVoTest().getUserId());
		assertEquals(orderInVoTest.getOrderVoTest().getRegDate(), targetOrderVoTest.getOrderVoTest().getRegDate());
		assertSame(orderInVoTest.getOrderVoTest().getRegDate(), targetOrderVoTest.getOrderVoTest().getRegDate());
		assertSame(orderInVoTest.getOrderVoTest(), targetOrderVoTest.getOrderVoTest());

		log.debug("targetOrderVoTest.getOrderItemVoTest {}", targetOrderVoTest.getOrderItemVoTest().toString());

		assertEquals(orderInVoTest.getOrderItemVoTest().getAddServiceId(),  targetOrderVoTest.getOrderItemVoTest().getAddServiceId());
		assertEquals(orderInVoTest.getOrderItemVoTest().getProductId(), targetOrderVoTest.getOrderItemVoTest().getProductId());
		assertSame(orderInVoTest.getOrderItemVoTest(), targetOrderVoTest.getOrderItemVoTest());

		assertSame(orderInVoTest, orderVoWrapper.get());
	}

	/**
	 * orderVoWrapper.convertChildObject
	 */
	@Test
	public void convertObjectChildTest() {
		OrderVoTest orderVoTest = orderVoWrapper.convertChildObject(OrderVoTest.class);
		log.debug("orderVoTest {}", orderVoTest.toString());

		assertEquals(orderInVoTest.getOrderVoTest().getAddress(), orderVoTest.getAddress());
		assertEquals(orderInVoTest.getOrderVoTest().getUserId(),  orderVoTest.getUserId());
		assertEquals(orderInVoTest.getOrderVoTest().getRegDate(), orderVoTest.getRegDate());
		assertSame(orderInVoTest.getOrderVoTest().getRegDate(), orderVoTest.getRegDate());
		assertNotSame(orderInVoTest.getOrderVoTest(), orderVoTest);

		OrderItemVoTest orderItemVoTest = orderVoWrapper.convertChildObject(OrderItemVoTest.class);
		log.debug("orderItemVoTest {}", orderItemVoTest.toString());

		assertEquals(orderInVoTest.getOrderItemVoTest().getAddServiceId(),  orderItemVoTest.getAddServiceId());
		assertEquals(orderInVoTest.getOrderItemVoTest().getProductId(), orderItemVoTest.getProductId());
		assertNotSame(orderInVoTest.getOrderItemVoTest(), orderItemVoTest);

		assertSame(orderInVoTest, orderVoWrapper.get());
	}


	/**
	 * orderVoWrapper.toHashMap
	 * orderVoWrapper.toHashMapUpperUnderscore
	 * orderVoWrapper.toMapLowerUnderscore
	 */
	@Test
	public void convertObjectMapTest() {
		log.debug("hashMap {}", orderVoWrapper.toHashMap());
		log.debug("map {}", orderVoWrapper.toMap());
		log.debug("toMapUpperUnderscore {}", orderVoWrapper.toMapUpperUnderscore());
		log.debug("toMapLowerUnderscore {}", orderVoWrapper.toMapLowerUnderscore());
	}

	/**
	 * orderVoWrapper.toHashMapChildObject
	 * orderVoWrapper.toMapChildObject
	 * orderVoWrapper.toHashMapUpperUnderscore(Child.class)
	 * orderVoWrapper.toMapLowerUnderscore(Child.class)
	 */
	@Test
	public void convertObjectChildMapTest() {
		log.debug("hashMap {}", orderVoWrapper.toHashMapChildObject(OrderVoTest.class));
		log.debug("map {}", orderVoWrapper.toMapChildObject(OrderVoTest.class));
		log.debug("toMapUpperUnderscore {}", orderVoWrapper.toMapUpperUnderscore(OrderVoTest.class));
		log.debug("toMapLowerUnderscore {}", orderVoWrapper.toMapLowerUnderscore(OrderVoTest.class));
	}

	/**
	 * fieldWrapper.toHashMap();
	 * fieldWrapper.toHashMapUpperUnderscore();
	 */
	@Test
	public void convertPublicFieldToMapTest() {
		PubTestVo result = new PubTestVo();
		result.rec_cnt1 = "1234";
		ObjectWrapper fieldWrapper = new ObjectWrapper(result, ObjectConvertUtil.ACCESS_TYPE_FIELD);

		Map map1 = fieldWrapper.toHashMap();
		log.debug("==> map1 :  {}", map1);
		log.debug("==> map1.get(\"rec_cnt1\") :  {}", map1.get("rec_cnt1"));
		assertEquals(result.rec_cnt1,  map1.get("rec_cnt1"));

		Map map2 = fieldWrapper.toHashMapUpperUnderscore();
		log.debug("==> map2 :  {}", map2);
		log.debug("==> map2.get(\"REC_CNT1\") :  {}", map2.get("REC_CNT1"));
		assertEquals(result.rec_cnt1,  map2.get("REC_CNT1"));
	}

	@Test
	public void convertObjectDefaultValue1Tests() {
		OrderVoTest orderVoTest       = new OrderVoTest();
		OrderVoTest  result = ObjectConvertUtil.toTargetObjectDefaultValue(orderVoTest, OrderVoTest.class);

		log.debug(">> result : {}", result);
		assertEquals(orderVoTest.getId(),  			0);
		assertEquals(orderVoTest.getUserName(),  	"");
		assertEquals(orderVoTest.getUserId(),  		"");
		assertEquals(orderVoTest.getAddress(),  	"");
		assertEquals(orderVoTest.getPhoneNumber(),  "");
		assertEquals(orderVoTest.getPrice(),  		0);
		assertEquals(orderVoTest.getMemo(),  		"");
		assertEquals(orderVoTest.getStatus(),  		"");
		assertEquals(orderVoTest.getRegDate(),  	null);
		assertEquals(orderVoTest.getUpdDate(),  	null);
		assertEquals(orderVoTest.getOrderItemId(),  0);
	}

	@Test
	public void convertObjectDefaultValue2Tests() {
		OrderVoTest orderVoTest       = new OrderVoTest();
		ObjectWrapper orderVoWrapper  = new ObjectWrapper(orderVoTest);
		OrderVoTest  result = orderVoWrapper.convertObjectDefaultValue(OrderVoTest.class);

		log.debug(">> result : {}", result);
		assertEquals(orderVoTest.getId(),  			0);
		assertEquals(orderVoTest.getUserName(),  	"");
		assertEquals(orderVoTest.getUserId(),  		"");
		assertEquals(orderVoTest.getAddress(),  	"");
		assertEquals(orderVoTest.getPhoneNumber(),  "");
		assertEquals(orderVoTest.getPrice(),  		0);
		assertEquals(orderVoTest.getMemo(),  		"");
		assertEquals(orderVoTest.getStatus(),  		"");
		assertEquals(orderVoTest.getRegDate(),  	null);
		assertEquals(orderVoTest.getUpdDate(),  	null);
		assertEquals(orderVoTest.getOrderItemId(),  0);
	}

	@Test
	public void convertChildObjectDefaultValueTests() {
		OrderInVoTest orderInVoTest   = new OrderInVoTest();
		orderInVoTest.setOrderItemVoTest(new OrderItemVoTest());
//		orderInVoTest.setOrderVoTest(new OrderVoTest());

		ObjectWrapper orderVoWrapper  = new ObjectWrapper(orderInVoTest);
		OrderItemVoTest  result1 = orderVoWrapper.convertChildObjectDefaultValue(OrderItemVoTest.class);
		OrderVoTest  result2 = orderVoWrapper.convertChildObjectDefaultValue(OrderVoTest.class);

		log.debug(">> OrderItemVoTest : {}", result1);
		log.debug(">> OrderVoTest : {}", result2);

		assertEquals(result1.getId(),  		 0);
		assertEquals(result1.getProductId(), "");
		assertEquals(result1.getRegDate(),   null);

		assertEquals(result2.getId(),  		 0);
		assertEquals(result2.getMemo(),  	 "");
		assertEquals(result2.getRegDate(),   null);
	}

}
