package com.skt.twd.order.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문 상세 View 사용되는 VO 이다.")
public class OrderDetailViewVoTest {

	@JsonProperty("order")
	private OrderVoTest orderVoTest;

	@JsonProperty("orderItem")
	private OrderItemVoTest orderItemVoTest;
}
