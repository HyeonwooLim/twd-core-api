package com.skt.twd.order.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel(description = "주문 요청시 사용되는 VO 이다.")
public class OrderInVoTest {

	@JsonProperty("order")
	private OrderVoTest orderVoTest;

	@JsonProperty("orderItem")
	private OrderItemVoTest orderItemVoTest;
}
