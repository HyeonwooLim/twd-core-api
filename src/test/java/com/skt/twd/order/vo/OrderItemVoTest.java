package com.skt.twd.order.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel(description = "주문 Item Entity")
public class OrderItemVoTest {
	@NotNull
	@ApiModelProperty(example = "1", required = true)
	private int id;

	@ApiModelProperty(notes = "상품 ID", example = "productId", required = true)
	private String productId;

	@ApiModelProperty(notes = "부가서비스 ID", example = "addServiceId", required = true)
	private String addServiceId;

	private Date regDate;
	private Date updDate;
}
