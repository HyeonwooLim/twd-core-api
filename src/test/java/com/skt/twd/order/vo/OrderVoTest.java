package com.skt.twd.order.vo;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.skt.twd.core.mvc.ValidMarker.Create;
import com.skt.twd.core.mvc.ValidMarker.Delete;
import com.skt.twd.core.mvc.ValidMarker.Update;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel(description = "주문 Entity")
public class OrderVoTest {
	@NotNull(groups = {Create.class, Update.class, Delete.class})
	private int id;

	@NotEmpty
    @Size(min = 1, max = 20)
	@ApiModelProperty(notes = "주문자 이름", example = "테스터", required = true)
	private String userName;

	@NotEmpty
    @Size(min = 1, max = 20)
	@ApiModelProperty(notes = "1 ~ 20 자리 이내 문자열을 사용한다.", example = "tester", required = true)
	private String userId;
	private String address;

	@NotEmpty(message="{valid.phoneNumber.notEmpty}", groups = {Create.class})
	@ApiModelProperty(notes = "'-' 없이 숫자로만 사용한다.", example = "01012349876", required = true)
	private String phoneNumber;

	@NotNull
	@Min(0)
	@Max(100)
	private int price;

	@ApiModelProperty(notes = "메모", example = "경비실에 맞겨주세요.", required = true)
	private String memo;

	@ApiModelProperty(notes = "주문상태", example = "START", required = true)
	private String status;

	private Date regDate;
	private Date updDate;

	@NotNull
	@ApiModelProperty(notes = "주문 Item ID", example = "1", required = true)
	private int orderItemId;
}
